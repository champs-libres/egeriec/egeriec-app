#!/bin/bash

cd $(dirname "${0}")

pandoc --from markdown \
     --number-sections \
     --pdf-engine=xelatex \
     --template=template.tex \
     --lua-filter=image-path.lua \
     -o documentation-egeriec-app.pdf \
     src/001-cover.md \
     src/002-toc.md \
     ../../egeriec-app.wiki/Introduction.md \
     ../../egeriec-app.wiki/Analyse.md \
     ../../egeriec-app.wiki/Installation.md \
     ../../egeriec-app.wiki/Dev/Generalities.md \
     ../../egeriec-app.wiki/Dev/Initial-data.md \
     ../../egeriec-app.wiki/Admin/Admin-interface.md \
     ../../egeriec-app.wiki/Admin/Map-layers-data.md \
     ../../egeriec-app.wiki/Diagnostic/Analyse-des-paramètres.md \
     ../../egeriec-app.wiki/Diagnostic/Information-land-use.md \
     ../../egeriec-app.wiki/OAD/Methodo1.md \
     ../../egeriec-app.wiki/OAD/Methodo2.md \
     ../../egeriec-app.wiki/OAD/Import-data.md \
     ../../egeriec-app.wiki/Perspectives.md \



