# Documentation de l'application

## Contenu

Le contenu de la documentation vit dans le wiki: https://gitlab.com/champs-libres/egeriec/egeriec-app/-/wikis/home. Éditer ce contenu en ligne ou en utilisant le dépôt du wiki.

## Build

Pour faire un beau pdf de cette documentation, on utilise pandoc avec un template. Pour lancer la commande:

```bash
documentation$ sudo sh build.sh
```

Note: pour utiliser le filtre lua sur les images, je dois faire cette commande avec sudo.

J'ai utilisé pandoc 2.14.2.

Pour que pandoc trouve les fichiers, il faut leur indiquer un chemin valide. Ici, on présuppose que les dépots de ce projet de du wiki sont tels que:

├── repo
│   └── egeriec-app
│   └── egeriec-app.wiki

Sinon, changer les chemins relatifs dans le fichier `build.sh`.