EGERIEC-app
===========

This is the repo for the EGERIEC web application. This application shows a
diagnostic of some water uptake stations and offers a decision making tool
(or "Outil d'aide à la décision" - OAD) for implementing nature-based solution
for improving water quality.

## Requirements

* [Go Task][go task website]
* [Docker][docker website]

You can use the list of tasks by doing `task -l`.

## Installation

1. Clone this repo

```bash
git clone git@gitlab.com:champs-libres/egeriec/egeriec-app.git
cd egeriec-app
```

2. Create the file `.env` and update it accordingly

```bash
cp .env.dist .env
```

3. Build an immutable production environment

```bash
task prod-up
```

4. Optionally, run the database migrations:

```bash
task make-migrations
```

5. Optionally, create the superuser:

```bash
task create-superuser
```

The app should be running at http://127.0.0.1:9000/

You can now connect to the admin with your credentials at
http://127.0.0.1:9000/admin

## Development

It is possible to run a development version of the app with some pre-configured
tools such as: [Adminer][adminer] and NodeJS.

To enable development mode, follow the same steps as in the installation,
but replace the step 3 with:

```bash
task dev-up
```

This will spawn new containers and update existing one to make the developer
experience a bit more friendly.

The development mode allows developer to hack the application. The containers
are mutable and allows you to quickly get into the application development.

The Adminer instance will live at http://127.0.0.1:9000/adminer/

Concerning NodeJS, there is nothing to do. Once the Javascript files are
modified, they will be "hot" compiled by Webpack devserver and the DJango app
will be reloaded automatically thanks to live-reloading in Webpack.

However, if you need to enter the NodeJS container for some reasons:

```bash
task dev-nodejs
```
For accessing the `backend` container, run:

```bash
task dev-backend
```

For rebuilding your containers, run:
 ```bash
 docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up --force-recreate --build --remove-orphans backend
```

## Tests

We used `vitest` for running test.

The test files are in the directory `frontend/test`

To run the tests, get access to the `node` container (`task dev-nodejs`) then run `npx vitest`

[go task website]: https://taskfile.dev
[docker website]: https://www.docker.com
[adminer]: https://www.adminer.org/en/
