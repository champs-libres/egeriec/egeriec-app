export const colors = {
    'blue': '#00789c',
    'blue-trend': '#0f0070',
    'blue-smooth': '#1802c5',
    'red': '#c10f0f',
    'red-trend': '#a90405',
    'red-smooth': '#ce0c0d',
    'orange': '#fd7e14',
    'brown': '#c04d08', //this is the complementary of blue #00789c
    'green': '#0fc10f',
    'dark-green': '#094b09',
    'black': '#000000',
    'grey': '#666666'
};
