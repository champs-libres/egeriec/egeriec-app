export const crumbs = [
    {
        id: 1,
        name: "Choix de méthodologie",
        desc: "Localisez le captage pour lequel vous souhaitez utiliser l'outil et répondez au questionnaire en vous basant sur le diagnostic de pollution."
    },
    {
        id: 2,
        name: "Pré-classement",
        desc: "Renseignez le polluant problématique."
    },
    {
        id: 3,
        name: "Dessin",
        desc: "Identifiez la zone de travail et les chemins de l'eau à l'aide de la fiche informative. Ensuite, dessinez des lignes où l'implantation de solutions semble être la plus pertinente.",
        descMethodo2: "Localisez l'axe d'écoulement concentré et dessinez des lignes où l'implantation de solutions semble être la plus pertinente. Maximum 3 lignes et une ligne par position (amont, milieu et aval). "
    },
    {
        id: 4,
        name: "Scénario",
        desc: "Choisissez le nombre de scénarii que vous souhaitez comparer et sélectionnez une solution par ligne pour chaque scénario en vérifiant l'adaptabilité au terrain via les couches cartographiques.",
        descMethodo2: "Choisissez le nombre de scénarii que vous souhaitez comparer, indiquez la longueur de l'axe d'écoulement et la surface du bassin versant contributif à l'axe et sélectionnez une solution par ligne pour chaque scénario en vérifiant l'adaptabilité au terrain via les couches cartographiques."
    },
    {
        id: 5,
        name: "Résultat",
        desc: "Observez/analysez les résultats. "
    },
];
