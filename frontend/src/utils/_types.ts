import { PositionType } from "@/calculator";
import {Feature} from "ol";

export type ContentType = 'map' | 'chart';
export type SidebarMode = 'info' | 'layers' | 'quality' | null;
export type SidebarContentType = 'diagnostic' | 'tool';
export type ResourceToProtectType = null | 'surface' | 'souterraine';
export type ModalType = null | 'permaLink' | 'legend' | 'graph-methods';
export type BackgroundLayer = null | 'osm' | 'ortho'
export type LineDrawn = {
    id: number,
    line: Feature,
    length: number,
    type?: string,
    solutionId?: number,
    position?: PositionType,
    flowPosition?: FlowPosition
};
export type Scenario = {
    id: number,
    lines: Array<LineDrawn>;
};

export type CrumbType = {
    id: number,
    name: string,
    desc: string,
    descMethodo2?: string
}
export type Trend = {
    date: Date,
    intercept: number,
    slope: number,
    significance: boolean
}

export type SmoothData = {
    date: Date,
    value: number
}

export type ChartDateY = {
    x: Date,
    y: number
}

export type plotlyTrace = { //TODO is there a type from Plotly?
    [key: string]: any;
}

export type ModalState = {
    isOpen: boolean,
    type: ModalType,
    title: string,
    content: string
}

export type AgriContext = 'grandes-cultures' | 'prairies';

export type FlowPosition = 'amont' | 'milieu' | 'aval';

export type MethodoType = null | 'methodo1' | 'methodo2';

export type MethodoFormStateType = 'init' | 'choixModePrincipal' | 'choixModePrincipalAmont' | 'reponse'

export type MethodoFormActionType = 'none' | 'methodo1' | 'methodo2' | 'subform' | 'print'

export type TransferModeType = {
    id: number,
    name: string,
    form_action: MethodoFormActionType,
    form_text: string,
}

export type CotationsDictType = { [solution:string]: { [criterion:string]: {'value': string} }}

export type Solution = {
    id: number,
    name: string,
    slug: string,
    description: string|null,
    methodology: string,
    exigences_d_implantation: string|null,
    spatial_constraint: string|null
}

export type Result = {
  value: number,
  weight: number,
  name: string,
  kind: string
}

export type AMCResults = {
    [key: string]: Result;
}

export type Weights = {
    [key: string]: number;
}

export type OrderedSolution = {
    id: number,
    name: string,
    efficacite: number,
    slug: string
};

export type ContentLegend = {
    id: number,
    name: string,
    description: string,
    type: string,
    color: string,
    imgURL: string
}

export type DrawingState = {
    isDrawing: boolean,
    type: 'lines' | 'axe' | 'zone' | ''
}
