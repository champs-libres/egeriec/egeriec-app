import { ResourceToProtectType, SmoothData, Solution, TransferModeType, Trend } from "@/utils/_types";

export interface Station {
  'id': number,
  'type': 'feature',
  'geometry': {
    'type': 'Point',
    'coordinates': number[]
  },
  'properties': {
    'name': string;
    'municipality': string;
    'elevation': number;
    'depth': string;
    'strainerDepth': string;
    'service': boolean;
    'aquifer': string;
    'aquiferKind': string;
    'kind': string;
    'preventionZone': string;
  }
}

export interface StationLandUse {
  'id': number,
  'station': number,
  'surfaceZone': number,
  'agriShare': number,
  'cropShare': number,
  'meadowShare': number,
  'forestShare': number,
  'builtShare': number,
  'buildingsAge': number,
  'precipitation': number,
  'slope': number,
  'numberCemeteries': number,
  'numberAgriBuildings': number,
  'numberNonConnectedBuildings': number,
}

export interface MapLayer {
  'id': number,
  'name': string,
  'description': string,
  'category': string,
  'type': string,
  'data': string,
  'style': string,
  'visibility': boolean
  'minzoom': number
}

export interface Parameter {
  'id': number,
  'name': string,
  'unit': string,
  'min_value': number,
  'max_value': number,
  'threshold': number,
  'ordering': number,
  'scope': string,
  'slug': string
}

export interface Record {
  'id': number,
  'value': number,
  'parameter': number,
  'station': number,
  'datetime': Date
}


interface FeatureCollection<T> {
  'type': 'FeatureCollection',
  'features': T[]
}


export const fetchStations = ():Promise<FeatureCollection<Station>> => fetch('/api/stations')
  .then(response => response.json())
  .then(data => data);

export const fetchMapLayers = ():Promise<MapLayer[]> => fetch('/api/map-layers')
  .then(response => response.json())
  .then(data => data);

export const fetchParameters = ():Promise<Parameter[]> => fetch('/api/parameters')
  .then(response => response.json())
  .then(data => data);

export const fetchRecords = (stationId: number, parameterId = null):Promise<Record[]> => {
  const url = parameterId ? `/api/records?station=${stationId}&parameter=${parameterId}` : `/api/records?station=${stationId}`
  return fetch(url)
    .then(response => response.json())
    .then(data => data);
  };

export const fetchAllStationRecords = (parameterId: number|null):Promise<Record[] | []> => {
  if (parameterId) {
    return fetch(`/api/records?parameter=${parameterId}`)
      .then(response => response.json())
      .then(data => data)
  } else {
    return new Promise(() => []);
  }
};

export const fetchTransferModes = (resourceToProtect: ResourceToProtectType):Promise<TransferModeType[]> => fetch(`/api/transferModes?resource=${resourceToProtect}`)
  .then(response => response.json())
  .then(data => data);

export const fetchTransferModesAmont = ():Promise<TransferModeType[]> => fetch(`/api/transferModes?resource=amont`)
  .then(response => response.json())
  .then(data => data);

export const fetchTrends = (stationId: number|null, parameterId: number|null):Promise<Trend[]> =>
  fetch(`/trends?station=${stationId}&parameter=${parameterId}`)
    .then(response => response.json())
    .then(data => data.results);

export const fetchSmooth = (stationId: number|null, parameterId: number|null):Promise<SmoothData[]> =>
    fetch(`/smooth?station=${stationId}&parameter=${parameterId}`)
      .then(response => response.json())
      .then(data => data.results);

export const fetchCotations = () => fetch('/api/cotations_by_solution_and_criterion')
  .then(response => response.json())
  .then(data => data);

export const fetchSolutions = ():Promise<Solution[]> => fetch('/api/solutions')
  .then(response => response.json())
  .then(data => data);

export const fetchStationsLandUse = ():Promise<StationLandUse[]> => fetch('/api/stations-landuse')
  .then(response => response.json())
  .then(data => data);

export const fetchNominatim = (input: string) =>
    fetch(`https://nominatim.openstreetmap.org/search.php?q=${input}&countrycodes=BE&format=json`)
      .then(response => response.json())
      .then(data => data)
