import {State} from "@/store";
import {ChartDateY, SmoothData, Trend} from "@/utils/_types";
import {colors} from '@/utils/_colors';
import {Store} from "vuex";
import {Parameter} from "@/api";

// TODO move getters to the store
export const getStationName = (store: Store<State>, stationId: number|null): string => stationId ?
  store.state.stations.filter(s => s.id === stationId)[0].properties.name : '';

export const getParameterLabel = (store: Store<State>, parameterId: number|null): string => parameterId ?
  `${store.state.parameters.filter(p => p.id === parameterId)[0].name} (${store.state.parameters.filter(p => p.id === parameterId)[0].unit})` : '';

export const getDataRange = (store: Store<State>) => {
    const arr = store.state.allStationsRecords.map(r => r.value).sort((a, b) => a - b);
    return [arr[0], arr[arr.length - 1]]
};

export const makeAllStationTrace = (store: Store<State>) => {
    return {
      mode: 'markers',
      type: 'scattergl',
      hoverinfo: 'none',
      opacity: 0.3,
      marker: {
          color: colors.grey,
          size: 2,
      },
      x: store.state.allStationsRecords.map(record => record.datetime),
      y: store.state.allStationsRecords.map(record => record.value),
      name: 'Mesures de toutes les stations'
  };
};

export const makeLastYearRecordHistogram = (store: Store<State>) => {
    return {
        type: 'histogram',
        x: store.state.allStationsAveragesLastYear,
        name: 'Les mesures de la dernière année pour chaque station',
        nbinsx: 100,
        marker: {
            color: colors.blue
        }
    }
}

export const makeStationTrace = (store: Store<State>) => {
    return {
      mode: "markers",
      opacity: 0.6,
      type: 'scattergl',
      hoverinfo: 'none',
      marker: {
          color: colors.blue,
          symbol: 'cross',
          size: 10,
      },
      x: store.state.filteredRecords.map(record => record.datetime),
      y: store.state.filteredRecords.map(record => record.value),
      name: getStationName(store, store.state.selectedStationId)
  };
};

export const makeExtraStationTrace = (store: Store<State>) => {
    return {
       mode: "markers",
       opacity: 0.6,
       marker: {
           color: colors.red,
           symbol: 'cross',
           size: 10,
       },
       x: store.state.filteredRecordsExtraStation.map(record => record.datetime),
       y: store.state.filteredRecordsExtraStation.map(record => record.value),
       name: getStationName(store, store.state.extraStationId)
   };
};

export const makeNormTrace = (store: Store<State>, parameterForNorm: Parameter) => {
    return {
      mode: "lines",
      x: store.state.filteredRecords.map(record => record.datetime),
      y: store.state.filteredRecords.map(() => parameterForNorm.threshold),
      line: {
          color: colors.black,
          width: 1.5,
      },
      name: "norme"
  };
};


const prepareTrendData = (store: Store<State>, trends: Trend[]): ChartDateY[] => {
    let data: ChartDateY[] = [];
    trends.map(
        t => {
        data.push({
            x: t.date,
            y: t.intercept
        })
        }
    )
    let endDataDate: Date = new Date(store.state.filteredRecords[store.state.filteredRecords.length - 1].datetime);
    let lastTrendDate = new Date(trends[trends.length - 1].date);
    data.push(
        {
        x: endDataDate,
        y: trends[trends.length-1].intercept +
            trends[trends.length-1].slope * (endDataDate.getTime() - lastTrendDate.getTime()) / (1000 * 3600 * 24 * 365)
        }
    )
    return data;
};

export const makeTrendTrace = (store: Store<State>, trendData: Trend[], color: string, stationId: number|null) => {
    return {
        mode: "lines",
        x: prepareTrendData(store, trendData).map(d => d.x),
        y: prepareTrendData(store, trendData).map(d => d.y),
        line: {
            color: color,
            width: 2,
            dash: "dash",
        },
        name: `tendances - ${getStationName(store, stationId)}`
    };
};

export const makeSmoothTrace = (smoothData: SmoothData[], color: string) => {
    return {
        mode: "lines",
        x: smoothData.map(d => d.date),
        y: smoothData.map(d => d.value),
        line: {
            color: color,
            width: 1,
        },
        name: `moyenne mobile (lowess)`
    };
};


