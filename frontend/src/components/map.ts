import Map from 'ol/Map' ;
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import OSM from 'ol/source/OSM';

import {State} from "@/store";
import {Store} from "vuex";

export const hasLayerByTitle = (map: Map, title: string): boolean => {
  let cond = false;
  map.getAllLayers().map(l => {
    if (l.get('title') === title) {
      cond = true
    }
  });
  return cond;
}

export const removeLayerByTitle = (map: Map, title: string) =>
  map.getAllLayers().map(l => {
    if (l.get('title') === title) {
      map.removeLayer(l);
    }
  });

export const toggleLayerVisibilityByTitle = (map: Map, title: string) =>
  map.getAllLayers().map(l => {
    if (l.get('title') === title) {
      l.setVisible(!l.getVisible());
    }
  });

export const removeSidebarLayers = (map: Map) =>
  map.getAllLayers().map(l => {
    if (l.get('sidebar')) {
      map.removeLayer(l);
    }
  });

export const osmLayer = new TileLayer({
  source: new OSM(),
  properties: {
    title: 'osm'
  }
});

export const orthoLayer = new TileLayer({
    source: new TileWMS({
        url: 'https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_LAST/MapServer/WMSServer',
        params: { 'LAYERS': '0', 'TILED': true },
        crossOrigin: 'Anonymous',
    }),
    properties: {
      title: 'ortho'
    }
});

export const initMapBackgroundLayer = (store: Store<State>) => {
  if (store.state.backgroundLayer === 'osm') {
    store.state.map.addLayer(osmLayer);
  } else {
    if (store.state.backgroundLayer === 'ortho') {
      store.state.map.addLayer(orthoLayer);
    }
  }
};

export const alterWmsLayer = (context) => {

  const canvas = context.canvas;
  const width = canvas.width;
  const height = canvas.height;

  const inputData = context.getImageData(0, 0, width, height).data;

  const output = context.createImageData(width, height);
  const outputData = output.data;

  for (let pixelY = 0; pixelY < height; ++pixelY) {
    for (let pixelX = 0; pixelX < width; ++pixelX) {

      const inputIndex =((width * pixelY) + pixelX) * 4;
      const r = inputData[inputIndex];
      const g = inputData[inputIndex + 1];
      const b = inputData[inputIndex + 2];
      const a = inputData[inputIndex + 3];

      if (r < 215) {
        outputData[inputIndex] = 0;
        outputData[inputIndex + 1] = 0;
        outputData[inputIndex + 2] = 0;
        outputData[inputIndex + 3] = 0;
      } else {
        outputData[inputIndex] = r;
        outputData[inputIndex + 1] = g;
        outputData[inputIndex + 2] = b;
        outputData[inputIndex + 3] = a;
      }
    }
  }

  context.putImageData(output, 0, 0);
}