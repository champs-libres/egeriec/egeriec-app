import { ResourceToProtectType, CotationsDictType, Scenario, AMCResults, Weights, AgriContext, MethodoType, FlowPosition } from "../utils/_types";
import { compute_psa } from "./psa";

export type PositionType = 'parcellaire' | 'long-cours-deau';

// represent the computation in Efficacite_1_D26
export const scenario_total_length = (scenario: Scenario) : number =>
    scenario.lines.reduce((result, current) => { return result + current.length }, 0)

// (Efficacité)D[32-43]
export const solution_ratio = (scenario: Scenario, solutionId: number, position: PositionType) => {
    // pour un scenario calcule le pourcentage de la solution
    const lines = scenario.lines.filter(line => line.position === position && line.solutionId === solutionId);

    let sumLongueur = 0;
    lines.forEach((line) => {
        sumLongueur = sumLongueur + line.length;
    });

    return sumLongueur / scenario_total_length(scenario);
};

// to access to the data
// - from the file data.ts
// - or from the http://localhost:9000/api/cotations_by_solution_and_criterion/ ( using the fetchCotations in the store )
const get_value = (data): number => {

    if (data !== undefined && data.hasOwnProperty('value')) {
        return Number(data.value)
    }
    return 0;
};


const getValueCriterion =
  (cotations: CotationsDictType|null, criterion: string, solutionSlug: string): number =>
    cotations ? get_value(cotations[solutionSlug][criterion]): 0;

/**
* Returns an "efficacité" value as a function of the polluant and a solution.
* Corresponds to the "Données AMC (1)", lines 4-9, columns F-L
*/
export const getEfficacite =
  (cotations: CotationsDictType|null, methodoNumber: string, polluantSlug: string|null, solutionSlug: string, flowPosition: FlowPosition = 'milieu'): number =>
  methodoNumber === '1'
      ? getValueCriterion(cotations, `efficacite-ecoulement-diffus-de-surface_${polluantSlug}`, solutionSlug)
      : getValueCriterion(cotations, `efficacite-rendement-delimination-par-rapport-au-transfert-vers-la-ressource_${polluantSlug}-${flowPosition}`, solutionSlug)

/** Compute the efficacite for only for methodo1 -> in the excel sheet it is : (Efficacité 1)D52
* cotations are the data from http://localhost:9000/api/cotations_by_solution_and_criterion/
* resource -> not handled for the moment (normaly, it won't be used - to be checked)
* polluant the polluant chosen (Préclassement 1)D5
*/
export const compute_efficacite_methodo1 = (
    cotations: CotationsDictType,
    resource: ResourceToProtectType,
    polluantSlug: string | null,
    scenario: Scenario
): number =>
    compute_methodo1(
        cotations,
        resource,
        scenario,
        `efficacite-ecoulement-diffus-de-surface_${polluantSlug}`
    );

export const compute_efficacite_methodo2 = (
    cotations: CotationsDictType,
    scenario: Scenario,
    polluantSlug: string | null,
): number => {

    let values: number[] = [];
    for (let l of scenario.lines) {
        if (l.solutionId) {
            let solutionSlug = getSolutionSlug(l.solutionId ?? 0);
            let flowPosition = l.flowPosition ?? 'milieu';
            let criterion = `efficacite-rendement-delimination-par-rapport-au-transfert-vers-la-ressource_${polluantSlug}-${flowPosition}`;
            values.push(getValueCriterion(cotations, criterion, solutionSlug))
        }
    }

    return values.reduce((a, b) => a + b, 0) / scenario.lines.length;
};

const compute_methodo1 = (
    cotations: CotationsDictType,
    resource: ResourceToProtectType,
    scenario: Scenario,
    criterion: string
): number => {

    const d32 = solution_ratio(scenario, 1, 'parcellaire'); // 'bande-enherbee_1'
    const d33 = solution_ratio(scenario, 1, 'long-cours-deau');
    const d34 = solution_ratio(scenario, 2, 'parcellaire'); // 'haie_1'
    const d35 = solution_ratio(scenario, 2, 'long-cours-deau');
    const d36 = solution_ratio(scenario, 3, 'parcellaire'); // 'haie-bande-enherbee_1'
    const d37 = solution_ratio(scenario, 3, 'long-cours-deau');
    const d38 = solution_ratio(scenario, 4, 'parcellaire'); // 'b-lignocellulosique-de-miscanthus_1'
    const d39 = solution_ratio(scenario, 4, 'long-cours-deau');
    const d40 = solution_ratio(scenario, 5, 'parcellaire'); // 'b-lignocellulosique-de-saulepeuplier_1'
    const d41 = solution_ratio(scenario, 5, 'long-cours-deau');
    const d42 = solution_ratio(scenario, 6, 'long-cours-deau'); // 'ripisylve_1'
    const d43 = solution_ratio(scenario, 7, 'long-cours-deau'); // 'ripisylve-bande-enherbee_1'

    const donnee_amc_1_fX = getValueCriterion(cotations, criterion, 'bande-enherbee_1');
    const donnee_amc_1_fX_2 = getValueCriterion(cotations, criterion, 'bande-enherbee_1');
    const donnee_amc_1_gX = getValueCriterion(cotations, criterion, 'haie_1');
    const donnee_amc_1_gX_2 = getValueCriterion(cotations, criterion, 'haie_1');
    const donnee_amc_1_hX = getValueCriterion(cotations, criterion, 'haie-bande-enherbee_1');
    const donnee_amc_1_hX_2 = getValueCriterion(cotations, criterion, 'haie-bande-enherbee_1');
    const donnee_amc_1_iX = getValueCriterion(cotations, criterion, 'b-lignocellulosique-de-miscanthus_1');
    const donnee_amc_1_iX_2 = getValueCriterion(cotations, criterion, 'b-lignocellulosique-de-miscanthus_1');
    const donnee_amc_1_jX = getValueCriterion(cotations, criterion, 'b-lignocellulosique-de-saulepeuplier_1');
    const donnee_amc_1_jX_2 = getValueCriterion(cotations, criterion, 'b-lignocellulosique-de-saulepeuplier_1');
    const donnee_amc_1_kX = getValueCriterion(cotations, criterion, 'ripisylve_1'); //TODO if parcellaire, do count nothing
    const donnee_amc_1_lX = getValueCriterion(cotations, criterion, 'ripisylve-bande-enherbee_1');

    return d32 * donnee_amc_1_fX
        + d33 * donnee_amc_1_fX_2
        + d34 * donnee_amc_1_gX
        + d35 * donnee_amc_1_gX_2
        + d36 * donnee_amc_1_hX
        + d37 * donnee_amc_1_hX_2
        + d38 * donnee_amc_1_iX
        + d39 * donnee_amc_1_iX_2
        + d40 * donnee_amc_1_jX
        + d41 * donnee_amc_1_jX_2
        + d42 * donnee_amc_1_kX
        + d43 * donnee_amc_1_lX;
}

/** should be from db, but the calculator aims at being independent... */
export const getSolutionSlug = (id:number): string => {
    switch (id) {
        case 8:
            return 'chenal-enherbe_2';
        case 9:
            return 'haie_2';
        case 10:
            return 'haie-bande-enherbee_2';
        case 11:
            return 'b-lignocellulosique-de-miscanthus_2';
        case 12:
            return 'bande-lignocellulosique-de-saule_2';
        case 13:
            return 'ripisylve_2';
        case 14:
            return 'ripisylve-bande-enherbee_2';
        case 15:
            return 'zone-tampon-humide-artificielle_2';
        default:
            return ''
    }
}

export const compute_methodo2 = (
    cotations: CotationsDictType,
    scenario: Scenario,
    criterion: string
): number => {
    let values: number[] = [];
    for (let l of scenario.lines) {
        if (l.solutionId) {
            let solutionSlug = getSolutionSlug(l.solutionId ?? 0);
            let value = 0;
            if (
                solutionSlug === 'chenal-enherbe_2' && (l.flowPosition === 'aval' || l.flowPosition === 'amont') ||
                solutionSlug === 'ripisylve_2' && (l.flowPosition === 'milieu' || l.flowPosition === 'amont') ||
                solutionSlug === 'ripisylve-bande-enherbee_2' && (l.flowPosition === 'milieu' || l.flowPosition === 'amont') ||
                solutionSlug === 'zone-tampon-humide-artificielle_2' && (l.flowPosition === 'milieu' || l.flowPosition === 'amont')
            ) {
                value = 0;
            } else {
                value = getValueCriterion(cotations, criterion, solutionSlug);
            }
            values.push(value);
        }
    }
    return values.reduce((a, b) => a + b, 0) / scenario.lines.length;
};

export const WEIGHT: Weights = {
  'efficacite': 36,
  'duree-de-vie': 12,
  'duree-necessaire-pour-atteindre-lefficacite-recherchee': 12,
  'couts-dinstallation': 3.75,
  'couts-dentretien': 3.75,
  'possibilite-de-valorisation': 4.5,
  'compensation-financiere-primes-plantation-maec_parcellaire': 3,
  'compensation-financiere-primes-plantation-maec_long-cours-deau': 3,
  'perte-de-surface-agricole-utilisable_parcellaire': 6,
  'perte-de-surface-agricole-utilisable_long-cours-deau': 6,
  'besoin-dentretiende-surveillancede-maintenance': 7.5,
  'besoin-daccord-du-proprietaire-des-terres-agricoles_parcellaire': 0.75,
  'besoin-daccord-du-proprietaire-des-terres-agricoles_long-cours-deau': 0.75,
  'besoin-dun-permis-durbanisme-en-zone-agricole_parcellaire': 0.75,
  'besoin-dun-permis-durbanisme-en-zone-agricole_long-cours-deau': 0.75,
  'effet-brise-vent-dombrage-microclimat': 0.75,
  'augmentation-de-la-fertilite-des-sols-des-cultures': 0.75,
  'refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_grandes-cultures': 0.75,
  'refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_prairies': 0.75,
  'impact-sur-lerosion': 0.75,
  'qualite-des-eaux-en-general': 2,
  'qualite-des-sols': 0.6,
  'qualite-de-lair': 0.4,
  'impact-sur-le-ruissellement-en-general-debit-et-volume': 0.25,
  'impact-sur-le-debordement-des-cours-deau-ralentissement-amont': 0.25,
  'habitat-pour-la-faune_grandes-cultures': 0.5,
  'habitat-pour-la-faune_prairies': 0.5,
  'abri-pour-la-flore_grandes-cultures': 0.5,
  'abri-pour-la-flore_prairies': 0.5,
  'corridor-ecologique_grandes-cultures': 0.5,
  'corridor-ecologique_prairies': 0.5,
  'sequestration-de-carbone': 1,
  'substitution-aux-energies-fossiles': 0.5,
  'paysage': 0.3,
  'loisir-agrotourisme': 0.1,
  'education': 0.1,
};

export const compute_amc =
(
    methodo: MethodoType,
    cotations: CotationsDictType,
    weighting: Weights,
    resource: ResourceToProtectType,
    scenario: Scenario,
    polluantSlug: string,
    position: PositionType = 'parcellaire',
    agriContext: AgriContext = 'grandes-cultures',
    coursDeau: boolean = true,
    flowLength: number = 300,
    watershedSurface: number = 10
): AMCResults => {

    if (methodo === 'methodo1') {
      return {
        'efficacite': {
            value: weighting['efficacite'] * compute_efficacite_methodo1(cotations, resource, polluantSlug, scenario),
            weight: weighting['efficacite'],
            name: 'Efficacité',
            kind: 'Efficacité et durée'
        },
        'duree-de-vie': {
            value: weighting['duree-de-vie'] * compute_methodo1(cotations, resource, scenario, 'duree-de-vie'),
            weight: weighting['duree-de-vie'],
            name: 'Durée de vie',
            kind: 'Efficacité et durée'
        },
        'duree-necessaire-pour-atteindre-lefficacite-recherchee': {
            value: weighting['duree-necessaire-pour-atteindre-lefficacite-recherchee'] * compute_methodo1(cotations, resource, scenario, 'duree-necessaire-pour-atteindre-lefficacite-recherchee'),
            weight: weighting['duree-necessaire-pour-atteindre-lefficacite-recherchee'],
            name: 'Durée nécessaire pour atteindre l\'efficacité recherchée',
            kind: 'Efficacité et durée'
        },
        'couts-dinstallation': {
            value: weighting['couts-dinstallation'] * compute_methodo1(cotations, resource, scenario, 'couts-dinstallation'),
            weight: weighting['couts-dinstallation'],
            name: 'Coûts d\'installation',
            kind: 'Financier'
        },
        'couts-dentretien': {
            value: weighting['couts-dentretien'] * compute_methodo1(cotations, resource, scenario, 'couts-dentretien'),
            weight: weighting['couts-dentretien'],
            name: 'Coûts d\'entretien',
            kind: 'Financier'
        },
        'possibilite-de-valorisation': {
            value: weighting['possibilite-de-valorisation'] * compute_methodo1(cotations, resource, scenario, 'possibilite-de-valorisation'),
            weight: weighting['possibilite-de-valorisation'],
            name: 'Possibilité de valorisation',
            kind: 'Financier'
        },
        'compensation-financiere-primes-plantation-maec': {
            value: weighting[`compensation-financiere-primes-plantation-maec_${position}`] * compute_methodo1(cotations, resource, scenario, `compensation-financiere-primes-plantation-maec_${position}`),
            weight: weighting[`compensation-financiere-primes-plantation-maec_${position}`],
            name: 'Compensation financière',
            kind: 'Financier'
        },
        'perte-de-surface-agricole-utilisable': {
            value: weighting[`perte-de-surface-agricole-utilisable_${position}`] * compute_methodo1(cotations, resource, scenario, `perte-de-surface-agricole-utilisable_${position}`),
            weight: weighting[`perte-de-surface-agricole-utilisable_${position}`],
            name: 'Perte de surface agricole',
            kind: 'Acceptabilité'
        },
        'besoin-dentretiende-surveillancede-maintenance': {
            value: weighting['besoin-dentretiende-surveillancede-maintenance'] * compute_methodo1(cotations, resource, scenario, 'besoin-dentretiende-surveillancede-maintenance'),
            weight: weighting['besoin-dentretiende-surveillancede-maintenance'],
            name: 'Besoin d\'entretien / maintenance',
            kind: 'Acceptabilité'
        },
        'besoin-daccord-du-proprietaire-des-terres-agricoles': {
            value: weighting[`besoin-daccord-du-proprietaire-des-terres-agricoles_${position}`] * compute_methodo1(cotations, resource, scenario, `besoin-daccord-du-proprietaire-des-terres-agricoles_${position}`),
            weight: weighting[`besoin-daccord-du-proprietaire-des-terres-agricoles_${position}`],
            name: 'Besoin d\'accord du propriétaire',
            kind: 'Acceptabilité'
        },
        'besoin-dun-permis-durbanisme-en-zone-agricole': {
            value: weighting[`besoin-dun-permis-durbanisme-en-zone-agricole_${position}`] * compute_methodo1(cotations, resource, scenario, `besoin-dun-permis-durbanisme-en-zone-agricole_${position}`),
            weight: weighting[`besoin-dun-permis-durbanisme-en-zone-agricole_${position}`],
            name: 'Besoin d\'un permis d\'urbanisme',
            kind: 'Acceptabilité'
        },
        'effet-brise-vent-dombrage-microclimat': {
            value: weighting['effet-brise-vent-dombrage-microclimat'] * compute_methodo1(cotations, resource, scenario, 'effet-brise-vent-dombrage-microclimat'),
            weight: weighting['effet-brise-vent-dombrage-microclimat'],
            name: 'Effet brise-vent, d\'ombrage, microclimat',
            kind: 'Avantages secondaires'
        },
        'augmentation-de-la-fertilite-des-sols-des-cultures': {
            value: weighting['augmentation-de-la-fertilite-des-sols-des-cultures'] * compute_methodo1(cotations, resource, scenario, 'augmentation-de-la-fertilite-des-sols-des-cultures'),
            weight: weighting['augmentation-de-la-fertilite-des-sols-des-cultures'],
            name: 'Augmentation de la fertilité des sols',
            kind: 'Avantages secondaires'
        },
        'refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs': {
            value: weighting[`refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_${agriContext}`] * compute_methodo1(cotations, resource, scenario, `refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_${agriContext}`),
            weight: weighting[`refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_${agriContext}`],
            name: 'Refuge pour les espèces de premier choix',
            kind: 'Avantages secondaires'
        },
        'impact-sur-lerosion': {
            value: weighting['impact-sur-lerosion'] * compute_methodo1(cotations, resource, scenario, 'impact-sur-lerosion'),
            weight: weighting['impact-sur-lerosion'],
            name: 'Impact sur l\'érosion',
            kind: 'Avantages secondaires'
        },
        'qualite-des-eaux-en-general': {
            value: weighting['qualite-des-eaux-en-general'] * compute_methodo1(cotations, resource, scenario, 'qualite-des-eaux-en-general'),
            weight: weighting['qualite-des-eaux-en-general'],
            name: 'Qualité des eaux',
            kind: 'Avantages secondaires'
        },
        'qualite-des-sols': {
            value: weighting['qualite-des-sols'] * compute_methodo1(cotations, resource, scenario, 'qualite-des-sols'),
            weight: weighting['qualite-des-sols'],
            name: 'Qualité des sols',
            kind: 'Avantages secondaires'
        },
        'qualite-de-lair': {
            value: weighting['qualite-de-lair'] * compute_methodo1(cotations, resource, scenario, 'qualite-de-lair'),
            weight: weighting['qualite-de-lair'],
            name: 'Qualité de l\'air',
            kind: 'Avantages secondaires'
        },
        'impact-sur-le-ruissellement-en-general-debit-et-volume': {
            value: weighting['impact-sur-le-ruissellement-en-general-debit-et-volume'] * compute_methodo1(cotations, resource, scenario, 'impact-sur-le-ruissellement-en-general-debit-et-volume'),
            weight: weighting['impact-sur-le-ruissellement-en-general-debit-et-volume'],
            name: 'Impact sur le ruissellement',
            kind: 'Avantages secondaires'
        },
        'impact-sur-le-debordement-des-cours-deau-ralentissement-amont': {
            value: weighting['impact-sur-le-debordement-des-cours-deau-ralentissement-amont'] * compute_methodo1(cotations, resource, scenario, 'impact-sur-le-debordement-des-cours-deau-ralentissement-amont'),
            weight: weighting['impact-sur-le-debordement-des-cours-deau-ralentissement-amont'],
            name: 'Impact sur le débordement des cours d\'eau',
            kind: 'Avantages secondaires'
        },
        'habitat-pour-la-faune': {
            value: weighting[`habitat-pour-la-faune_${agriContext}`] * compute_methodo1(cotations, resource, scenario, `habitat-pour-la-faune_${agriContext}`),
            weight: weighting[`habitat-pour-la-faune_${agriContext}`],
            name: 'Habitat pour la faune',
            kind: 'Avantages secondaires'
        },
        'abri-pour-la-flore': {
            value: weighting[`abri-pour-la-flore_${agriContext}`] * compute_methodo1(cotations, resource, scenario, `abri-pour-la-flore_${agriContext}`),
            weight: weighting[`abri-pour-la-flore_${agriContext}`],
            name: 'Abri pour la flore',
            kind: 'Avantages secondaires'
        },
        'corridor-ecologique': {
            value: weighting[`corridor-ecologique_${agriContext}`] * compute_methodo1(cotations, resource, scenario, `corridor-ecologique_${agriContext}`),
            weight: weighting[`corridor-ecologique_${agriContext}`],
            name: 'Corridor écologique',
            kind: 'Avantages secondaires'
        },
        'sequestration-de-carbone': {
            value: weighting['sequestration-de-carbone'] * compute_methodo1(cotations, resource, scenario, 'sequestration-de-carbone'),
            weight: weighting['sequestration-de-carbone'],
            name: 'Séquestration de carbone',
            kind: 'Avantages secondaires'
        },
        'substitution-aux-energies-fossiles': {
            value: weighting['substitution-aux-energies-fossiles'] * compute_methodo1(cotations, resource, scenario, 'substitution-aux-energies-fossiles'),
            weight: weighting['substitution-aux-energies-fossiles'],
            name: 'Substitution aux énergies fossiles',
            kind: 'Avantages secondaires'
        },
        'paysage': {
            value: weighting['paysage'] * compute_methodo1(cotations, resource, scenario, 'paysage'),
            weight: weighting['paysage'],
            name: 'Paysage',
            kind: 'Avantages secondaires'
        },
        'loisir-agrotourisme': {
            value: weighting['loisir-agrotourisme'] * compute_methodo1(cotations, resource, scenario, 'loisir-agrotourisme'),
            weight: weighting['loisir-agrotourisme'],
            name: 'Loisir - agrotourisme',
            kind: 'Avantages secondaires'
        },
        'education': {
            value: weighting['education'] * compute_methodo1(cotations, resource, scenario, 'education'),
            weight: weighting['education'],
            name: 'Éducation',
            kind: 'Avantages secondaires'
        }
      };
    } else {
        return {
            'efficacite': {
                value: weighting['efficacite'] * compute_efficacite_methodo2(cotations, scenario, polluantSlug),
                weight: weighting['efficacite'],
                name: 'Efficacité',
                kind: 'Efficacité et durée'
            },
            'duree-de-vie': {
                value: weighting['duree-de-vie'] * compute_methodo2(cotations, scenario, 'duree-de-vie'),
                weight: weighting['duree-de-vie'],
                name: 'Durée de vie',
                kind: 'Efficacité et durée'
            },
            'duree-necessaire-pour-atteindre-lefficacite-recherchee': {
                value: weighting['duree-necessaire-pour-atteindre-lefficacite-recherchee'] * compute_methodo2(cotations, scenario, 'duree-necessaire-pour-atteindre-lefficacite-recherchee'),
                weight: weighting['duree-necessaire-pour-atteindre-lefficacite-recherchee'],
                name: 'Durée nécessaire pour atteindre l\'efficacité recherchée',
                kind: 'Efficacité et durée'
            },
            'couts-dinstallation': {
                value: weighting['couts-dinstallation'] * compute_methodo2(cotations, scenario, 'couts-dinstallation'),
                weight: weighting['couts-dinstallation'],
                name: 'Coûts d\'installation',
                kind: 'Financier'
            },
            'couts-dentretien': {
                value: weighting['couts-dentretien'] * compute_methodo2(cotations, scenario, 'couts-dentretien'),
                weight: weighting['couts-dentretien'],
                name: 'Coûts d\'entretien',
                kind: 'Financier'
            },
            'possibilite-de-valorisation': {
                value: weighting['possibilite-de-valorisation'] * compute_methodo2(cotations, scenario, 'possibilite-de-valorisation'),
                weight: weighting['possibilite-de-valorisation'],
                name: 'Possibilité de valorisation',
                kind: 'Financier'
            },
            'compensation-financiere-primes-plantation-maec': {
                value: weighting[`compensation-financiere-primes-plantation-maec_${position}`] * compute_methodo2(cotations, scenario, `compensation-financiere-primes-plantation-maec`),
                weight: weighting[`compensation-financiere-primes-plantation-maec_${position}`],
                name: 'Compensation financière',
                kind: 'Financier'
            },
            'perte-de-surface-agricole-utilisable': {
                value: weighting[`perte-de-surface-agricole-utilisable_${position}`] * compute_psa(scenario, coursDeau, flowLength, watershedSurface),
                weight: weighting[`perte-de-surface-agricole-utilisable_${position}`],
                name: 'Perte de surface agricole',
                kind: 'Acceptabilité'
            },
            'besoin-dentretiende-surveillancede-maintenance': {
                value: weighting['besoin-dentretiende-surveillancede-maintenance'] * compute_methodo2(cotations, scenario, 'besoin-dentretiende-surveillancede-maintenance'),
                weight: weighting['besoin-dentretiende-surveillancede-maintenance'],
                name: 'Besoin d\'entretien / maintenance',
                kind: 'Acceptabilité'
            },
            'besoin-daccord-du-proprietaire-des-terres-agricoles': {
                value: weighting[`besoin-daccord-du-proprietaire-des-terres-agricoles_${position}`] * compute_methodo2(cotations, scenario, `besoin-daccord-du-proprietaire-des-terres-agricoles`),
                weight: weighting[`besoin-daccord-du-proprietaire-des-terres-agricoles_${position}`],
                name: 'Besoin d\'accord du propriétaire',
                kind: 'Acceptabilité'
            },
            'besoin-dun-permis-durbanisme-en-zone-agricole': {
                value: weighting[`besoin-dun-permis-durbanisme-en-zone-agricole_${position}`] * compute_methodo2(cotations, scenario, `besoin-dun-permis-durbanisme-en-zone-agricole`),
                weight: weighting[`besoin-dun-permis-durbanisme-en-zone-agricole_${position}`],
                name: 'Besoin d\'un permis d\'urbanisme',
                kind: 'Acceptabilité'
            },
            'effet-brise-vent-dombrage-microclimat': {
                value: weighting['effet-brise-vent-dombrage-microclimat'] * compute_methodo2(cotations, scenario, 'effet-brise-vent-dombrage-microclimat'),
                weight: weighting['effet-brise-vent-dombrage-microclimat'],
                name: 'Effet brise-vent, d\'ombrage, microclimat',
                kind: 'Avantages secondaires'
            },
            'augmentation-de-la-fertilite-des-sols-des-cultures': {
                value: weighting['augmentation-de-la-fertilite-des-sols-des-cultures'] * compute_methodo2(cotations, scenario, 'augmentation-de-la-fertilite-des-sols-des-cultures'),
                weight: weighting['augmentation-de-la-fertilite-des-sols-des-cultures'],
                name: 'Augmentation de la fertilité des sols',
                kind: 'Avantages secondaires'
            },
            'refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs': {
                value: weighting[`refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_${agriContext}`] * compute_methodo2(cotations, scenario, `refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_${agriContext}`),
                weight: weighting[`refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs_${agriContext}`],
                name: 'Refuge pour les espèces de premier choix',
                kind: 'Avantages secondaires'
            },
            'impact-sur-lerosion': {
                value: weighting['impact-sur-lerosion'] * compute_methodo2(cotations, scenario, 'impact-sur-lerosion'),
                weight: weighting['impact-sur-lerosion'],
                name: 'Impact sur l\'érosion',
                kind: 'Avantages secondaires'
            },
            'qualite-des-eaux-en-general': {
                value: weighting['qualite-des-eaux-en-general'] * compute_methodo2(cotations, scenario, 'qualite-des-eaux-en-general'),
                weight: weighting['qualite-des-eaux-en-general'],
                name: 'Qualité des eaux',
                kind: 'Avantages secondaires'
            },
            'qualite-des-sols': {
                value: weighting['qualite-des-sols'] * compute_methodo2(cotations, scenario, 'qualite-des-sols'),
                weight: weighting['qualite-des-sols'],
                name: 'Qualité des sols',
                kind: 'Avantages secondaires'
            },
            'qualite-de-lair': {
                value: weighting['qualite-de-lair'] * compute_methodo2(cotations, scenario, 'qualite-de-lair'),
                weight: weighting['qualite-de-lair'],
                name: 'Qualité de l\'air',
                kind: 'Avantages secondaires'
            },
            'impact-sur-le-ruissellement-en-general-debit-et-volume': {
                value: weighting['impact-sur-le-ruissellement-en-general-debit-et-volume'] * compute_methodo2(cotations, scenario, 'impact-sur-le-ruissellement-en-general-debit-et-volume'),
                weight: weighting['impact-sur-le-ruissellement-en-general-debit-et-volume'],
                name: 'Impact sur le ruissellement',
                kind: 'Avantages secondaires'
            },
            'impact-sur-le-debordement-des-cours-deau-ralentissement-amont': {
                value: weighting['impact-sur-le-debordement-des-cours-deau-ralentissement-amont'] * compute_methodo2(cotations, scenario, 'impact-sur-le-debordement-des-cours-deau-ralentissement-amont'),
                weight: weighting['impact-sur-le-debordement-des-cours-deau-ralentissement-amont'],
                name: 'Impact sur le débordement des cours d\'eau',
                kind: 'Avantages secondaires'
            },
            'habitat-pour-la-faune': {
                value: weighting[`habitat-pour-la-faune_${agriContext}`] * compute_methodo2(cotations, scenario, `habitat-pour-la-faune_${agriContext}`),
                weight: weighting[`habitat-pour-la-faune_${agriContext}`],
                name: 'Habitat pour la faune',
                kind: 'Avantages secondaires'
            },
            'abri-pour-la-flore': {
                value: weighting[`abri-pour-la-flore_${agriContext}`] * compute_methodo2(cotations, scenario, `abri-pour-la-flore_${agriContext}`),
                weight: weighting[`abri-pour-la-flore_${agriContext}`],
                name: 'Abri pour la flore',
                kind: 'Avantages secondaires'
            },
            'corridor-ecologique': {
                value: weighting[`corridor-ecologique_${agriContext}`] * compute_methodo2(cotations, scenario, `corridor-ecologique_${agriContext}`),
                weight: weighting[`corridor-ecologique_${agriContext}`],
                name: 'Corridor écologique',
                kind: 'Avantages secondaires'
            },
            'sequestration-de-carbone': {
                value: weighting['sequestration-de-carbone'] * compute_methodo2(cotations, scenario, 'sequestration-de-carbone'),
                weight: weighting['sequestration-de-carbone'],
                name: 'Séquestration de carbone',
                kind: 'Avantages secondaires'
            },
            'substitution-aux-energies-fossiles': {
                value: weighting['substitution-aux-energies-fossiles'] * compute_methodo2(cotations, scenario, 'substitution-aux-energies-fossiles'),
                weight: weighting['substitution-aux-energies-fossiles'],
                name: 'Substitution aux énergies fossiles',
                kind: 'Avantages secondaires'
            },
            'paysage': {
                value: weighting['paysage'] * compute_methodo2(cotations, scenario, 'paysage'),
                weight: weighting['paysage'],
                name: 'Paysage',
                kind: 'Avantages secondaires'
            },
            'loisir-agrotourisme': {
                value: weighting['loisir-agrotourisme'] * compute_methodo2(cotations, scenario, 'loisir-agrotourisme'),
                weight: weighting['loisir-agrotourisme'],
                name: 'Loisir - agrotourisme',
                kind: 'Avantages secondaires'
            },
            'education': {
                value: weighting['education'] * compute_methodo2(cotations, scenario, 'education'),
                weight: weighting['education'],
                name: 'Éducation',
                kind: 'Avantages secondaires'
            }
        };
    }
};


export const compute_amc_for_scenarios =
(
    methodo: MethodoType,
    cotations: CotationsDictType,
    weighting: Weights,
    resource: ResourceToProtectType,
    scenarii: Scenario[],
    polluantSlug: string,
    agriContext: AgriContext,
    coursDeau: boolean,
    flowLength: number,
    watershedSurface: number
): AMCResults[] => {
    let results: AMCResults[] = [];
    for (const s of scenarii) {
        let r = compute_amc(methodo, cotations, weighting, resource, s, polluantSlug, 'parcellaire', agriContext, coursDeau, flowLength, watershedSurface);
        results.push(r)
    }
    return results;
};