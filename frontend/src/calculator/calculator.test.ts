import { expect, test } from 'vitest'
import { scenario_total_length, solution_ratio, compute_efficacite_methodo1, compute_amc, WEIGHT } from './index'
import { scenario1, line1, solutionNotInScenario, positionNotInScenario, line4, scenario2 } from './tests'
import { cotations } from './data' // this is a copy from http://localhost:9000/api/cotations_by_solution_and_criterion/ with data from end of September 2022


test('Sum of scenario', () => {
    expect(scenario_total_length(scenario1)).toBe(2200)
    expect(solution_ratio(scenario1, line1.solutionId ?? 0, line1.position ?? 'parcellaire')).toBe(850 / 2200)
    expect(solution_ratio(scenario1, line4.solutionId ?? 0, line4.position ?? 'parcellaire')).toBe(300 / 2200) // sum of line3 + line4
    expect(solution_ratio(scenario1, solutionNotInScenario, positionNotInScenario)).toBe(0)
});

/** METHODO1 */

test('Efficacite', () => {
    expect(Math.abs(compute_efficacite_methodo1(cotations, null, 'no3', scenario1) - 0.25)).toBeLessThan(0.01)
    expect(Math.abs(compute_efficacite_methodo1(cotations, null, 'ppp-dissous', scenario1) - 0.49)).toBeLessThan(0.01)
    expect(Math.abs(compute_efficacite_methodo1(cotations, null, 'ppp-adsorbe', scenario1) - 0.61)).toBeLessThan(0.01)
    expect(Math.abs(compute_efficacite_methodo1(cotations, null, 'mes', scenario1) - 0.46)).toBeLessThan(0.01)
    expect(Math.abs(compute_efficacite_methodo1(cotations, null, 'mo', scenario1) - 0.51)).toBeLessThan(0.01)
    expect(Math.abs(compute_efficacite_methodo1(cotations, null, 'p', scenario1) - 0.68)).toBeLessThan(0.01)
});

test('Efficacite globale', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['efficacite']['value'] - 8.8)).toBeLessThan(0.1)
});

test('Duree de vie', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['duree-de-vie']['value'] - 7.3)).toBeLessThan(0.1)
});

test('Durée nécessaire pour atteindre l efficacité recherchée', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['duree-necessaire-pour-atteindre-lefficacite-recherchee']['value'] - 7.1)).toBeLessThan(0.1)
});

test('Coûts d installation', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['couts-dinstallation']['value'] - 2.5)).toBeLessThan(0.1)
});

test('Coûts d entretien', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['couts-dentretien']['value'] - 2.1)).toBeLessThan(0.1)
});

test('Possibilité de valorisation', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['possibilite-de-valorisation']['value'] - 3.2)).toBeLessThan(0.1)
});

test('Compensation financière (primes plantation + MAEC)', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['compensation-financiere-primes-plantation-maec']['value'] - 1.5)).toBeLessThan(0.1)
});

test('Compensation financière (primes plantation + MAEC) -long de l eau', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3', 'long-cours-deau')['compensation-financiere-primes-plantation-maec']['value'] - 1.5)).toBeLessThan(0.1)
});

test('Perte de surface agricole utilisable', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['perte-de-surface-agricole-utilisable']['value'] - 3.2)).toBeLessThan(0.1)
});

test('Besoin d entretien', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['besoin-dentretiende-surveillancede-maintenance']['value'] - 4.7)).toBeLessThan(0.1)
});

test('Besoin d accord proprietaire', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['besoin-daccord-du-proprietaire-des-terres-agricoles']['value'] - 0.6)).toBeLessThan(0.1)
});

test('Besoin permis urbanisme', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['besoin-dun-permis-durbanisme-en-zone-agricole']['value'] - 0.6)).toBeLessThan(0.1)
});

test('Effet brise vent', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['effet-brise-vent-dombrage-microclimat']['value'] - 0.2)).toBeLessThan(0.1)
});

test('Augmentation fertilité', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['augmentation-de-la-fertilite-des-sols-des-cultures']['value'] - 0.2)).toBeLessThan(0.1)
});

test('Refuge', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs']['value'] - 0.4)).toBeLessThan(0.1)
});

test('Erosion', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['impact-sur-lerosion']['value'] - 0.3)).toBeLessThan(0.1)
});

test('Qualité eaux', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['qualite-des-eaux-en-general']['value'] - 0.7)).toBeLessThan(0.1)
});

test('Qualité sol', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['qualite-des-sols']['value'] - 0.1)).toBeLessThan(0.1)
});

test('Qualité air', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['qualite-de-lair']['value'] - 0.1)).toBeLessThan(0.1)
});

test('Impact ruissellement', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['impact-sur-le-ruissellement-en-general-debit-et-volume']['value'] - 0.0)).toBeLessThan(0.1)
});

test('Impact débordement', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['impact-sur-le-debordement-des-cours-deau-ralentissement-amont']['value'] - 0.0)).toBeLessThan(0.1)
});

test('habitat pour la faune', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['habitat-pour-la-faune']['value'] - 0.3)).toBeLessThan(0.1)
});

test('abri-pour-la-flore', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['abri-pour-la-flore']['value'] - 0.3)).toBeLessThan(0.1)
});

test('corridor-ecologique', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['corridor-ecologique']['value'] - 0.2)).toBeLessThan(0.1)
});

test('sequestration-de-carbone', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['sequestration-de-carbone']['value'] - 0.3)).toBeLessThan(0.1)
});

test('substitution-aux-energies-fossiles', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['substitution-aux-energies-fossiles']['value'] - 0.2)).toBeLessThan(0.1)
});

test('paysage', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['paysage']['value'] - 0.0)).toBeLessThan(0.1)
});

test('loisir-agrotourisme', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['loisir-agrotourisme']['value'] - 0.0)).toBeLessThan(0.1)
});

test('education', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3')['education']['value'] - 0.0)).toBeLessThan(0.1)
});

test('Refuge grandes cultures', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3', 'parcellaire', 'grandes-cultures')['refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs']['value'] - 0.4)).toBeLessThan(0.1)
});

test('Refuge prairies', () => {
    expect(Math.abs(compute_amc('methodo1', cotations, WEIGHT, null, scenario1, 'no3', 'parcellaire', 'prairies')['refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs']['value'] - 0.2)).toBeLessThan(0.1)
});

/** METHODO2 */

test('Efficacite M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['efficacite']['value'] - 18.0)).toBeLessThan(0.1)
});

test('Duree de vie M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['duree-de-vie']['value'] - 3.6)).toBeLessThan(0.1)
});

test('Durée nécessaire pour atteindre l efficacité recherchée M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['duree-necessaire-pour-atteindre-lefficacite-recherchee']['value'] - 4.8)).toBeLessThan(0.1)
});

test('Coûts d installation M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['couts-dinstallation']['value'] - 3.7)).toBeLessThan(0.1)
});

test('Coûts d entretien M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['couts-dentretien']['value'] - 3.5)).toBeLessThan(0.1)
});

test('Possibilité de valorisation M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['possibilite-de-valorisation']['value'] - 3.0)).toBeLessThan(0.1)
});

test('Compensation financière (primes plantation + MAEC) M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['compensation-financiere-primes-plantation-maec']['value'] - 0)).toBeLessThan(0.1)
});

test('Perte de surface agricole utilisable M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['perte-de-surface-agricole-utilisable']['value'] - 4.85)).toBeLessThan(0.1)
});

test('Besoin d entretien M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['besoin-dentretiende-surveillancede-maintenance']['value'] - 4.8)).toBeLessThan(0.1)
});

test('Besoin d accord proprietaire M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['besoin-daccord-du-proprietaire-des-terres-agricoles']['value'] - 0.8)).toBeLessThan(0.1)
});

test('Besoin permis urbanisme M2', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['besoin-dun-permis-durbanisme-en-zone-agricole']['value'] - 0.5)).toBeLessThan(0.1)
});

test('Effet brise vent', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['effet-brise-vent-dombrage-microclimat']['value'] - 0.2)).toBeLessThan(0.1)
});

test('Augmentation fertilité', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['augmentation-de-la-fertilite-des-sols-des-cultures']['value'] - 0.1)).toBeLessThan(0.1)
});

test('Refuge', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['refuge-pour-les-especes-de-premier-choix-auxiliaires-et-pollinisateurs']['value'] - 0.3)).toBeLessThan(0.1)
});

test('Erosion', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['impact-sur-lerosion']['value'] - 0.2)).toBeLessThan(0.1)
});

test('Qualité eaux', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['qualite-des-eaux-en-general']['value'] - 1.2)).toBeLessThan(0.1)
});

test('Qualité sol', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['qualite-des-sols']['value'] - 0.1)).toBeLessThan(0.1)
});

test('Qualité air', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['qualite-de-lair']['value'] - 0.2)).toBeLessThan(0.1)
});

test('Impact ruissellement', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['impact-sur-le-ruissellement-en-general-debit-et-volume']['value'] - 0.0)).toBeLessThan(0.1)
});

test('Impact débordement', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['impact-sur-le-debordement-des-cours-deau-ralentissement-amont']['value'] - 0.1)).toBeLessThan(0.1)
});

test('habitat pour la faune', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['habitat-pour-la-faune']['value'] - 0.2)).toBeLessThan(0.1)
});

test('abri-pour-la-flore', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['abri-pour-la-flore']['value'] - 0.2)).toBeLessThan(0.1)
});

test('corridor-ecologique', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['corridor-ecologique']['value'] - 0.2)).toBeLessThan(0.1)
});

test('sequestration-de-carbone', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['sequestration-de-carbone']['value'] - 0.13)).toBeLessThan(0.1)
});

test('substitution-aux-energies-fossiles', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['substitution-aux-energies-fossiles']['value'] - 0.3)).toBeLessThan(0.1)
});

test('paysage', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['paysage']['value'] - 0.12)).toBeLessThan(0.1)
});

test('loisir-agrotourisme', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['loisir-agrotourisme']['value'] - 0.04)).toBeLessThan(0.1)
});

test('education', () => {
    expect(Math.abs(compute_amc('methodo2', cotations, WEIGHT, null, scenario2, 'no3')['education']['value'] - 0.01)).toBeLessThan(0.1)
});

