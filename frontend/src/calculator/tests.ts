import { LineDrawn, Scenario } from '../utils/_types';
import { Feature } from 'ol';
import { LineString } from 'ol/geom';

export const line1: LineDrawn = {
  id: 1,
  line: new Feature({
    geometry: new LineString([[0, 1], [1, 1]])
  }),
  position: 'parcellaire',
  length: 850,
  solutionId: 1 //'Bande enherbée'
};

export const line2: LineDrawn = {
  id: 2,
  line: new Feature({
    geometry: new LineString([[0, 0], [0, 1]])
  }),
  position: 'long-cours-deau',
  length: 750,
  solutionId: 4 // 'Bande lignocellulosique de miscanthus'
};

export const line3: LineDrawn = {
  id: 3,
  line: new Feature({
    geometry: new LineString([[1, 1], [1, 2]])
  }),
  position: 'parcellaire',
  length: 200,
  solutionId: 2 //'Haie'
}

export const line4: LineDrawn = {
  id: 4,
  line: new Feature({
    geometry: new LineString([[1, 1], [2, 1]])
  }),
  position: 'parcellaire',
  length: 100,
  solutionId: 2 //'Haie'
};

export const  line5: LineDrawn = {
  id: 5,
  line: new Feature({
    geometry: new LineString([[2, 1], [1, 2]])
  }),
  position: 'long-cours-deau',
  length: 300,
  solutionId: 7 //'Ripisylve + bande enherbée'
};

export const positionNotInScenario = 'long-cours-deau';
export const solutionNotInScenario = 99999;

export const scenario1: Scenario = {
  id: 1,
  lines: [line1, line2, line3, line4, line5]
};

const line6: LineDrawn = {
  id: 6,
  line: new Feature({
    geometry: new LineString([[2, 1], [1, 2]])
  }),
  length: 100,
  solutionId: 11, //Miscanthus
  flowPosition: 'amont'
};

const line7: LineDrawn = {
  id: 7,
  line: new Feature({
    geometry: new LineString([[2, 1], [1, 2]])
  }),
  length: 100,
  solutionId: 12, //Saule
  flowPosition: 'milieu'
};

const line8: LineDrawn = {
  id: 8,
  line: new Feature({
    geometry: new LineString([[2, 1], [1, 2]])
  }),
  length: 100,
  solutionId: 14, //Ripisylve + bande enherbée
  flowPosition: 'aval'
};

export const scenario2: Scenario = {
  id: 1,
  lines: [line6, line7, line8]
};



