import { getSolutionSlug } from ".";
import { Scenario } from "../utils/_types";


const COTATIONS_PSA_COURS_DEAU = [
    {
        solution: 'chenal-enherbe_2',
        value: 0
    },
    {
        solution: 'haie_2',
        value: 1
    },
    {
        solution: 'haie-bande-enherbee_2',
        value: 0.48
    },
    {
        solution: 'b-lignocellulosique-de-miscanthus_2',
        value: 0.87
    },
    {
        solution: 'bande-lignocellulosique-de-saule_2',
        value: 0.87
    },
    {
        solution: 'ripisylve_2',
        value: 1
    },
    {
        solution: 'ripisylve-bande-enherbee_2',
        value: 0.48
    },
    {
        solution: 'zone-tampon-humide-artificielle_2',
        value: 0
    },
];

/** m2 */
const SURFACE_PSA = [
    {
        solution: 'chenal-enherbe_2',
        value: undefined
    },
    {
        solution: 'haie_2',
        value: 150
    },
    {
        solution: 'haie-bande-enherbee_2',
        value: 750
    },
    {
        solution: 'b-lignocellulosique-de-miscanthus_2',
        value: 300
    },
    {
        solution: 'bande-lignocellulosique-de-saule_2',
        value: 300
    },
    {
        solution: 'ripisylve_2',
        value: 150
    },
    {
        solution: 'ripisylve-bande-enherbee_2',
        value: 750
    },
    {
        solution: 'zone-tampon-humide-artificielle_2',
        value: 1000
    },
];

/**
 * Matches Données AMC (2) G59:G66 cells
 */
const getValuePsaCoursDeau = (solutionSlug: string): number =>
    COTATIONS_PSA_COURS_DEAU.find(c => c.solution === solutionSlug)?.value ?? 0;

/**
 * Matches Données AMC (2) I59:I66 cells
 */
const getValuePsaSansCoursDeau = (solutionSlug: string, flowLength: number, watershedSurface: number): number => {
    let surface = 0;
    let CHENAL_ENHERBE_WIDTH = 20;
    let surfaceChenalEnherbe = CHENAL_ENHERBE_WIDTH * flowLength;
    if (solutionSlug === 'chenal-enherbe_2') {
        surface = surfaceChenalEnherbe;
    }
    if (solutionSlug === 'zone-tampon-humide-artificielle_2') {
        surface = 0.01 * watershedSurface * 10000 ;
    } else {
        surface = SURFACE_PSA.find(c => c.solution === solutionSlug)?.value ?? 0;
    }

    let minSurface = Math.min(...SURFACE_PSA.map(s => s.value ?? 0), surfaceChenalEnherbe);
    let maxSurface = Math.max(...SURFACE_PSA.map(s => s.value ?? 0), surfaceChenalEnherbe);

    let deltaSurface = minSurface - maxSurface;

    return (surface - maxSurface) / deltaSurface;
};


/**
* Compute the "Perte de surface agricole" for the methodo2
*/
export const compute_psa = (scenario: Scenario, coursDeau: boolean, flowLength: number, watershedSurface: number): number => {
    let values: number[] = [];
    for (let l of scenario.lines) {
      let solutionSlug = getSolutionSlug(l.solutionId ?? 0);
      if (l.solutionId) {
        if (l.flowPosition === 'aval' && coursDeau) {
            values.push(getValuePsaCoursDeau(solutionSlug));
        } else {
            values.push(getValuePsaSansCoursDeau(solutionSlug, flowLength, watershedSurface));
        }
      }
    }
    return values.reduce((a, b) => a + b, 0) / scenario.lines.length;
};
