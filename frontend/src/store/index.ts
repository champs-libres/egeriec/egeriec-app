import {
  fetchAllStationRecords,
  fetchCotations,
  fetchMapLayers,
  fetchParameters,
  fetchRecords,
  fetchSmooth,
  fetchSolutions,
  fetchStations,
  fetchStationsLandUse,
  fetchTransferModes,
  fetchTransferModesAmont,
  fetchTrends,
  MapLayer,
  Parameter,
  Record,
  Station,
  StationLandUse
} from '@/api'
import {Map} from 'ol'
import {InjectionKey} from 'vue'
import {createStore, Store, useStore as baseUseStore} from 'vuex'
import {
  AgriContext,
  AMCResults,
  BackgroundLayer,
  ContentType,
  CotationsDictType,
  CrumbType, DrawingState,
  FlowPosition,
  LineDrawn,
  MethodoFormStateType,
  MethodoType,
  ModalState,
  ResourceToProtectType,
  Scenario,
  SidebarContentType,
  SidebarMode,
  SmoothData,
  Solution,
  TransferModeType,
  Trend
} from "@/utils/_types";
import {crumbs} from "@/utils/_crumbs";
import {colors} from "@/utils/_colors";
import { compute_amc_for_scenarios, PositionType, WEIGHT } from '@/calculator';

export interface State {
  content: ContentType,
  sidebarContent: SidebarContentType,
  sidebarMode: SidebarMode,
  showResults: boolean,
  stations: Station[],
  stationsLandUse: StationLandUse[],
  parameters: Parameter[],
  mapLayers: MapLayer[],
  selectedStationId: null | number,
  extraStationId: null | number,
  selectedParameter: null | number,
  timeInterval: number[],
  records: Record[],
  allStationsRecords: Record[],
  allStationsAveragesLastYear: number[],
  filteredRecords: Record[],
  extraStationRecords: Record[],
  filteredRecordsExtraStation: Record[],
  map: Map,
  backgroundLayer: BackgroundLayer,
  resourceToProtect: ResourceToProtectType,
  showNorm: boolean,
  trendCheckbox: boolean,
  stationParameterTrend: Trend[],
  extraStationParameterTrend: Trend[],
  smoothData: SmoothData[],
  extraSmoothData: SmoothData[],
  transferModes: TransferModeType[],
  transferModesAmont: TransferModeType[],
  selectedTransferMode: null | number,
  selectedTransferModeAmont: null | number,
  currentCrumb: CrumbType,
  activeCrumbs: number[],
  pollutant: null | number,
  drawState: DrawingState,
  linesDrawn: LineDrawn[],
  scenarii: Scenario[],
  agriContext: null | AgriContext,
  flowLength: number,
  watershedSurface: number,
  coursDeau: boolean,
  currentZoom: number,
  currentCenter: number[],
  message: string,
  methodoFormState: MethodoFormStateType,
  methodoFormMessage: string,
  selectedMethodo: MethodoType,
  modalState: ModalState,
  selectedLayers: MapLayer[],
  cotations: CotationsDictType | null,
  solutions: Solution[],
  validatedStep: boolean,
  results: null | AMCResults[],
  spinner: number
}

export const key: InjectionKey<Store<State>> = Symbol()

export const store = createStore<State>({
  state: {
    content: 'map',
    sidebarContent: 'diagnostic',
    sidebarMode: 'info',
    showResults: false,
    stations: [],
    stationsLandUse: [],
    parameters: [],
    mapLayers: [],
    selectedStationId: null,
    extraStationId: null,
    selectedParameter: 1,
    timeInterval: [1964, new Date().getFullYear()],
    records: [],
    allStationsRecords: [],
    allStationsAveragesLastYear: [],
    filteredRecords: [],
    extraStationRecords: [],
    filteredRecordsExtraStation: [],
    map: new Map({}),
    backgroundLayer: 'osm' as BackgroundLayer,
    resourceToProtect: null,
    showNorm: false,
    trendCheckbox: false,
    stationParameterTrend: [],
    extraStationParameterTrend: [],
    smoothData: [],
    extraSmoothData: [],
    transferModes: [],
    transferModesAmont: [],
    selectedTransferMode: null,
    selectedTransferModeAmont: null,
    currentCrumb: crumbs[0],
    activeCrumbs: [1],
    pollutant: null,
    drawState: {
      isDrawing: false,
      type: ''
    },
    linesDrawn: [],
    scenarii: [],
    agriContext: null,
    flowLength: 300,
    watershedSurface: 10,
    coursDeau: true,
    currentZoom: 9,
    currentCenter: [485016.44, 6550857.95],
    message: '',
    methodoFormState: 'init',
    methodoFormMessage: '',
    selectedMethodo: null,
    modalState: {
      isOpen: false,
      type: null,
      title: '',
      content: ''
    },
    selectedLayers: [],
    cotations: null,
    solutions: [],
    validatedStep: false,
    results: null,
    spinner: 0
  },
  mutations: {
    /**
     * GENERAL MUTATIONS
     */
    setStations(state, payload: Station[]) {
      state.stations = payload;
    },
    setRecords(state, payload: Record[]) {
      state.records = payload;
    },
    setParameters(state, payload: Parameter[]) {
      state.parameters = payload;
    },
    setMessage(state, payload: string) {
      state.message = payload;
    },
    switchContent(state, payload: ContentType) {
      state.content = payload;
    },
    incrementSpinner(state) {
      state.spinner = state.spinner + 1;
    },
    decrementSpinner(state) {
      if (0 < state.spinner) {
        state.spinner = state.spinner - 1;
      }
    },
    catchError(state, payload) {
      console.log('Catch an error: ', payload);
    },
    /**
     * SIDEBAR MUTATIONS
     */
    switchSidebarContent(state, payload: SidebarContentType) {
      state.sidebarContent = payload;
    },
    showResults(state, payload: boolean) {
      state.showResults = payload;
    },
    setSidebarMode(state, payload: SidebarMode) {
      state.sidebarMode = payload;
    },
    setSelectedParameter(state, payload: number) {
      state.selectedParameter = payload;
    },
    setSelectedStationId(state, payload: number) {
      state.selectedStationId = payload;
    },
    setStationsLandUse(state, payload: StationLandUse[]) {
      state.stationsLandUse = payload;
    },
    /**
     * MAP MUTATIONS
     */
    setBackgroundLayer(state, payload: BackgroundLayer) {
      state.backgroundLayer = payload
    },
    setMapLayers(state, payload: MapLayer[]) {
      state.mapLayers = payload;
    },
    setCurrentZoom(state, payload: number) {
      state.currentZoom = payload;
    },
    setCurrentCenter(state, payload: number[]) {
      state.currentCenter = payload;
    },
    setSelectedLayer(state, payload: MapLayer) {
      state.selectedLayers = [...state.selectedLayers, payload];
    },
    removeLayer(state, payload: MapLayer) {
      const indexToRemove = state.selectedLayers.indexOf(payload);
      state.selectedLayers = state.selectedLayers.filter((layer, index) => index != indexToRemove);
    },
    /**
     * MODAL MUTATIONS
     */
    setModalState(state, payload: ModalState) {
      state.modalState = payload;
    },
    /**
     * GRAPH MUTATIONS
     */
    setTimeInterval(state, payload: number[]) {
      state.timeInterval = payload;
    },
    setExtraSelectedStation(state, payload: number) {
      state.extraStationId = payload;
    },
    setExtraStationRecords(state, payload: Record[]) {
      state.extraStationRecords = payload;
    },
    setAllStationsRecords(state, payload: Record[]) {
      state.allStationsRecords = payload;
    },
    setAveragesLastYear(state, payload: number[]) {
      state.allStationsAveragesLastYear = payload;
    },
    setFilteredRecordsMainStation(state, payload: Record[]) {
      state.filteredRecords = payload;
    },
    setFilteredRecordsExtraStation(state, payload: Record[]) {
      state.filteredRecordsExtraStation = payload;
    },
    setShowNorm(state, payload: boolean) {
      state.showNorm = payload;
    },
    setTrendCheckbox(state, payload: boolean) {
      state.trendCheckbox = payload;
    },
    setStationParameterTrends(state, payload: Trend[]) {
      state.stationParameterTrend = payload;
    },
    setExtraStationParameterTrends(state, payload: Trend[]) {
      state.extraStationParameterTrend = payload;
    },
    setSmoothData(state, payload) {
      state.smoothData = payload;
    },
    setExtraStationSmoothData(state, payload) {
      state.extraSmoothData = payload;
    },
    setResults(state, payload: AMCResults[]) {
      state.results = payload;
    },
    /**
     * TOOL MUTATIONS
     */
    addSolutionToLine(state, payload: {scenarioId: number, lineId: number, solutionId: number|null}){
      const scenarioToUpdate = state.scenarii.find(s => s.id === payload.scenarioId);
      if (scenarioToUpdate) {
        const lineToUpdate = scenarioToUpdate.lines.find(l => l.id === payload.lineId);
        if (lineToUpdate) {
          if (payload.solutionId) {
            Object.assign(lineToUpdate, {solutionId: payload.solutionId});
            let position: PositionType = [6, 7].includes(payload.solutionId) ? 'long-cours-deau' : 'parcellaire'; //TODO be able to choose this for some solutions, if really needed
            Object.assign(lineToUpdate, {position: position});
          } else {
            Object.assign(lineToUpdate, {solutionId: null, position: null});
          }
          let lines = scenarioToUpdate.lines.filter(l => l.id !== payload.lineId);
          lines.push(lineToUpdate);
          Object.assign(scenarioToUpdate, {lines: lines})
        }
        state.scenarii = state.scenarii.filter(s => s.id !== payload.scenarioId)
        state.scenarii.push(scenarioToUpdate)
      }
    },
    addFlowPositionToLine(state, payload: {flowPosition: FlowPosition, lineId: number}){ // Add the flowPosition to ALL scenarios
      state.scenarii = state.scenarii.map(s => {
        const lineToUpdate = s.lines.find(l => l.id === payload.lineId);
        if (lineToUpdate) {
          Object.assign(lineToUpdate, {flowPosition: payload.flowPosition});
          let lines = s.lines.filter(l => l.id !== payload.lineId);
          lines.push(lineToUpdate);
          Object.assign(s, {lines: lines})
        }
        return s;
      })

    },
    setAgriContext(state, payload: AgriContext) {
      state.agriContext = payload;
    },
    setResourceToProtect(state, payload: ResourceToProtectType) {
      state.resourceToProtect = payload;
    },
    setTransferModes(state, payload: TransferModeType[]) {
      state.transferModes = payload;
    },
    setTransferModesAmont(state, payload: TransferModeType[]) {
      state.transferModesAmont = payload;
    },
    setSelectedTransferMode(state, payload: number) {
      state.selectedTransferMode = payload;
    },
    setSelectedTransferModeAmont(state, payload: number) {
      state.selectedTransferModeAmont = payload;
    },
    setActivatedCrumbs(state, payload: number) {
      state.activeCrumbs.push(payload);
    },
    setCurrentCrumb(state, payload: CrumbType) {
      state.currentCrumb = payload;
    },
    setPollutant(state, payload: number) {
      state.pollutant = payload;
    },
    setDrawState(state, payload: DrawingState) {
      state.drawState = payload;
    },
    setLines(state, payload: LineDrawn[]) {
      state.linesDrawn = payload;
    },
    setNewLine(state, payload: LineDrawn) {
      state.linesDrawn.push(payload);
    },
    deleteLine(state, payload: number) {
      state.linesDrawn = state.linesDrawn.filter(l => l.id !== payload);
    },
    setScenarii(state, payload: Scenario[]) {
      state.scenarii = payload;
    },
    setFlowLength(state, payload: number) {
      state.flowLength = payload;
    },
    setWatershedSurface(state, payload: number) {
      state.watershedSurface = payload;
    },
    setCoursDeau(state, payload: boolean) {
      state.coursDeau = payload;
    },
    setMethodoFormState(state, formState: MethodoFormStateType) {
      state.methodoFormState = formState;
    },
    setMethodoFormMessage(state, methodoFormMessage: string) {
      state.methodoFormMessage = methodoFormMessage;
    },
    setSelectedMethodo(state, methodo: MethodoType) {
      state.selectedMethodo = methodo;
    },
    setCotations(state, payload) {
      state.cotations = payload;
    },
    setSolutions(state, payload) {
      state.solutions = payload;
    },
    setValidatedStep(state, payload: boolean) {
      state.validatedStep = payload;
    },
  },
  getters: {
    getTimeIntervalRecords: () => (records: Record[]): number[] => {
      const years = records.map((record) => new Date(record.datetime).getFullYear())
      return years.length !== 0 ? [years[0], years[years.length - 1]] : [1960, new Date().getFullYear()];
    },
    getFilteredRecords: (state) => (records: Record[]): Record[] => {
      const filteredRecords = records.filter((record) => {
        const year = new Date(record.datetime).getFullYear();
        const timeInterval = state.timeInterval.length === 0 ? [1960, new Date().getFullYear()] : [state.timeInterval[0], state.timeInterval[1]];
        return (year >= timeInterval[0] && year <= timeInterval[1]);
      }).filter((record) => record.parameter === state.selectedParameter);

      const isAllNull = filteredRecords.every((record) => record.value === 0);

      return isAllNull ? [] : filteredRecords;
    },
    getLastYearAverages: (state) => (records: Record[]): number[] => {
      let lastYearAverageAllStations: number[] = [];
      let allStationIds = state.stations.map((station) => station.id)

      allStationIds.forEach((stationId) => {
        let recordsByStation = records.filter((record) => record.station === stationId && record.parameter === state.selectedParameter);
        let recordDates = recordsByStation.map((record) => record.datetime);

        if (recordsByStation.length !== 0) {
          let lastYearRecordsByStation = recordsByStation.filter((record: Record) => {
            const lastYear = new Date(recordDates[recordDates.length - 1]);
            const recordDate = new Date(record.datetime);
            let oneYearEarlier = new Date(recordDates[recordDates.length - 1]);
            oneYearEarlier = new Date(oneYearEarlier.setFullYear(oneYearEarlier.getFullYear() - 1));

            return recordDate >= oneYearEarlier && recordDate <= lastYear;
          });
          let lastYearRecordsValueAverage = (lastYearRecordsByStation.map((record: Record) => record.value)
              .reduce((previousValue: number, currentValue: number) => previousValue + currentValue, 0)) / lastYearRecordsByStation.length

          lastYearAverageAllStations.push(lastYearRecordsValueAverage);
        }
      })

      return lastYearAverageAllStations;
    },
    getListExtraStations: (state): Station[] => {
      return state.stations.filter((station) => station.id !== store.state.selectedStationId);
    },
    getDiagnosticParameters: (state): Parameter[] => {
      return state.parameters.filter((parameter) => parameter.scope[0] === 'dia');
    },
    getPollutants: (state): Parameter[] => {
      return state.parameters.filter((parameter) => parameter.scope[0] === 'aod');
    },
    getParameterUnit: (state): string => {
        return state.parameters.filter(p => p.id === store.state.selectedParameter)[0].unit
    },
    getLastYearAverage: (state): string => {
      const lastYearAverage = (state.allStationsAveragesLastYear
          .reduce((previousValue: number, currentValue: number ) => previousValue + currentValue, 0 )) / state.allStationsAveragesLastYear.length

      return lastYearAverage.toPrecision(3);
    },
    getDataLastYear: (state): number => {
      const recordsMainStationByParameter = state.records.filter(r => r.parameter === state.selectedParameter)
      const dates = recordsMainStationByParameter.map((record) => record.datetime);
      return new Date(dates[dates.length - 1]).getFullYear();
    },
    getLastYearAverageMainStation: (state) : number => {
      const recordsMainStationByParameter = state.records.filter(r => r.parameter === state.selectedParameter)
      const dates = recordsMainStationByParameter.map((record) => record.datetime);

      const lastYearRecords = recordsMainStationByParameter.filter((r) => {
        const lastYear = new Date(dates[dates.length - 1]);
        const recordDate = new Date(r.datetime);
        let oneYearEarlier = new Date(dates[dates.length - 1]);
        oneYearEarlier = new Date(oneYearEarlier.setFullYear(oneYearEarlier.getFullYear() - 1));

        return recordDate >= oneYearEarlier && recordDate <= lastYear;
      })

      console.log(lastYearRecords)
      const lastYearAverageMainStation = (lastYearRecords.map((record: Record) => record.value)
          .reduce((previousValue: number, currentValue: number) => previousValue + currentValue, 0)) / lastYearRecords.length;

      return parseFloat(lastYearAverageMainStation.toPrecision(3));
    },
    getNormLabel: (state, getters): string => {
       return `${state.parameters.filter(p => p.id === state.selectedParameter)[0].threshold} ${getters.getParameterUnit}`
    },
    getTrendColor: () => (slope: number, significance: boolean): string =>
      significance
        ? slope > 0 ? colors.red : colors.green
        : 'white',
    getTrendAngle: () => (slope: number, significance: boolean): string =>
      significance
        ? slope > 0 ? 'rotate(45deg)' : 'rotate(135deg)'
        : 'rotate(90deg)',
    getTrendYearMonth: () => (date: Date): string =>
      date.toLocaleDateString('fr-FR', {year: 'numeric', month: 'long'})
  },
  actions: {
    getStations({ commit, dispatch }) {
      commit('incrementSpinner');
      fetchStations()
        .then(response => commit('setStations', response.features))
        .then(() => dispatch('getAllStationsRecords'))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    getStationsLandUse({ commit }) {
      commit('incrementSpinner');
      fetchStationsLandUse()
        .then(response => commit('setStationsLandUse', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); })
    },
    getMapLayers({ commit }) {
      commit('incrementSpinner');
      fetchMapLayers()
        .then(response => commit('setMapLayers', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    getParameters({ commit }) {
      commit('incrementSpinner');
      fetchParameters()
        .then(response => commit('setParameters', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    getRecords({ state, commit, getters, dispatch }) {
      if (state.selectedStationId) {
        commit('incrementSpinner');
        fetchRecords(state.selectedStationId)
          .then(response => {
            commit('setRecords', response)
          })
          .then(() => {
            const timeIntervalRecords = getters.getTimeIntervalRecords(state.records)
            commit('setTimeInterval', [timeIntervalRecords[0], timeIntervalRecords[1]]);
            if (state.records.length === 0) {
              commit('setMessage', "Il n'y a pas de données à afficher");
            } else {
              commit('setFilteredRecordsMainStation', getters.getFilteredRecords(state.records));
            }
          })
          .then(() => commit('decrementSpinner'))
          .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
        dispatch('getSmoothData');
      }
    },
    getExtraRecords({state, commit, getters, dispatch}) {
      if (state.extraStationId) {
        commit('incrementSpinner');
        fetchRecords(state.extraStationId)
          .then(response => {
            commit('setExtraStationRecords', response);
          })
          .then(() => {
            const timeIntervalRecords = getters.getTimeIntervalRecords(state.extraStationRecords)
            commit('setTimeInterval', [timeIntervalRecords[0], timeIntervalRecords[1]]);
            commit('setFilteredRecordsExtraStation', getters.getFilteredRecords(state.extraStationRecords));
          })
          .then(() => commit('decrementSpinner'))
          .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
        dispatch('getSmoothData');
      }
    },
    getAllStationsRecords({ state, commit, getters }) {
      commit('incrementSpinner');
      fetchAllStationRecords(state.selectedParameter)
        .then(response => {
          commit('setAllStationsRecords', response);
          commit('setAveragesLastYear', getters.getLastYearAverages(response));
        })
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    getTrends({state, commit}) {
      commit('incrementSpinner');
      fetchTrends(state.selectedStationId, state.selectedParameter)
        .then(response => {
          commit('setStationParameterTrends', response)
        })
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); })
      ;
      if(state.extraStationId) {
        fetchTrends(state.extraStationId, state.selectedParameter)
          .then(response => {
            commit('setExtraStationParameterTrends', response)
          })
          .then(() => commit('decrementSpinner'))
          .catch((e) => { commit('decrementSpinner'); commit('catchError', e); })
        ;
      }
    },
    getSmoothData({state, commit}) {
      commit('incrementSpinner');
      fetchSmooth(state.selectedStationId, state.selectedParameter)
        .then(response => {
          commit('setSmoothData', response)
        })
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
      if(state.extraStationId) {
        commit('incrementSpinner');
        fetchSmooth(state.extraStationId, state.selectedParameter)
          .then(response => {
            commit('setExtraStationSmoothData', response)
          })
          .then(() => commit('decrementSpinner'))
          .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
      }
    },
    getCotations({ commit }) {
      commit('incrementSpinner');
      fetchCotations()
        .then(response => commit('setCotations', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    getSolutions({ commit }) {
      commit('incrementSpinner');
      fetchSolutions()
        .then(response => commit('setSolutions', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    computeResults({ state, commit }){
      if (state.selectedMethodo && state.cotations && state.selectedParameter && state.scenarii && state.agriContext) {
        const results = compute_amc_for_scenarios(
          state.selectedMethodo,
          state.cotations,
          WEIGHT,
          null,
          state.scenarii,
          state.parameters.filter((p) => p.id === Number(state.selectedParameter))[0]?.slug,
          state.agriContext,
          state.coursDeau,
          state.flowLength,
          state.watershedSurface
        );
        commit('setResults', results);
      }
    },
    setResourceToProtect({ commit, dispatch }, payload: ResourceToProtectType) {
      commit('setResourceToProtect', payload);
      dispatch('getTransferModes');
    },
    setFilteredRecordsMainStation({state, commit, getters}) {
      if (getters.getFilteredRecords(state.records).length !== 0) {
        commit('setMessage', "");
      } else {
        commit('setMessage', "il n'y a pas de données à afficher");
      }
      // first set newly filtered records
      commit('setFilteredRecordsMainStation', getters.getFilteredRecords(state.records));

      // then adjust time interval accordingly
      if (state.timeInterval[0] > getters.getTimeIntervalRecords(state.filteredRecords)[0] ||
          state.timeInterval[1] < getters.getTimeIntervalRecords(state.filteredRecords)[1] ||
          state.timeInterval.length === 0)
      {
        commit('setTimeInterval', [getters.getTimeIntervalRecords(state.filteredRecords)[0],
                getters.getTimeIntervalRecords(state.filteredRecords)[1]]);
      }
    },
    setFilteredRecordsExtraStation({state, commit, getters}) {
      commit('setFilteredRecordsExtraStation', getters.getFilteredRecords(state.extraStationRecords));
    },
    getTransferModes({ state, commit }) {
      commit('incrementSpinner');
      fetchTransferModes(state.resourceToProtect)
        .then(response => commit('setTransferModes', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    getTransferModesAmont({ commit }) {
      commit('incrementSpinner');
      fetchTransferModesAmont()
        .then(response => commit('setTransferModesAmont', response))
        .then(() => commit('decrementSpinner'))
        .catch((e) => { commit('decrementSpinner'); commit('catchError', e); });
    },
    setValidatedStep({ state, commit }) {
      switch (state.currentCrumb.id) {
        case 1:
          if (null !== state.resourceToProtect && null !== state.selectedTransferMode && null !== state.selectedMethodo) {
            commit('setValidatedStep', true);
          } else {
            commit('setMessage', "Vous devez choisir une ressource à protéger et un mode de transfert");
            commit('setValidatedStep', false);
          }
          break;
        case 2:
          if (null !== state.pollutant) {
            commit('setValidatedStep', true);
          } else {
            commit('setMessage', "Vous devez choisir un polluant")
            commit('setValidatedStep', false);
          }
          break;
        case 4:
          if (state.selectedMethodo === 'methodo1' ) {
            if (state.scenarii.every(s => s.lines.every(l => l.solutionId ? true : false))) {
              commit('setValidatedStep', true);
            } else {
              commit('setValidatedStep', false);
            }
          } else {
            commit('setValidatedStep', true);
          }
          break;
        default:
          commit('setValidatedStep', true);
          break;
      }
    },
    resetToolState({commit}) {
      commit('setScenarii', []);
      commit('setAgriContext', null);
      commit('setResourceToProtect', null);
      commit('setSelectedTransferMode', null);
      commit('setSelectedTransferModeAmont', null);
      commit('setActivatedCrumbs', [1]);
      commit('setCurrentCrumb', crumbs[0]);
      commit('setPollutant', null);
      commit('setIsDrawing', false);
      commit('setLines', []);
      commit('setMethodoFormMessage', '');
      commit('setMethodoFormState', 'init');
      commit('setValidatedStep', false);
      commit('setSelectedMethodo', null);
    },
    resetGraphState({commit}) {
      commit('setSelectedStationId', null);
      commit('setFilteredRecordsMainStation', []);
      commit('setExtraSelectedStation', null);
      commit('setSelectedParameter', 1);
      commit('showResults', false);
      commit('setStationParameterTrends', []);
      commit('setExtraStationParameterTrends', []);
      commit('setTrendCheckbox', false);
      commit('setTimeInterval', [1960, new Date().getFullYear()]);
    },
    resetStore({ commit, dispatch }) {
      dispatch('resetToolState');
      dispatch('resetGraphState');
      // store.state.map.getView().setZoom(9);
      // store.state.map.getView().setCenter([485016.44, 6550857.95]);

      commit('switchContent', 'map');
      commit('setSidebarMode', 'info');
      commit('switchSidebarContent', 'diagnostic');
      commit('setBackgroundLayer', 'osm');
      commit('setMessage', "");
    }
  }
})

store.dispatch('getStations');
store.dispatch('getStationsLandUse');
store.dispatch('getTransferModesAmont'); // TODO METTRE AILLEURS ?
store.dispatch('getMapLayers');
store.dispatch('getParameters');
store.dispatch('getCotations');
store.dispatch('getSolutions');
// store.dispatch('getAllStationsRecords');

export function useStore () {
  return baseUseStore(key)
}
