import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ToolView from '../views/ToolView.vue'
import Modal from "@c/Modal.vue";

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    children: [
      {
        path: 'modal',
        component: Modal
      }
    ]
  },
  {
    path: '/:zoom/:center',
    name: 'permalink',
    component: HomeView,
    props: true
  },
  {
    path: '/decision-tool',
    name: 'decision-tool',
    component: ToolView
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
