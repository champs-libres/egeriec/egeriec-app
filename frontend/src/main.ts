import { createApp } from 'vue'
import { store, key } from './store'
import App from './App.vue'
import router from './router'
import '@/assets/base.scss'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'ol/ol.css'

createApp(App).use(router).use(store, key).mount('#app');
