const path = require('path')

const TsconfigPathsPlugin  = require('tsconfig-paths-webpack-plugin')
const { VueLoaderPlugin }  = require('vue-loader')

module.exports = (env) => {
  const IS_PROD_MODE  = env.production === true

  return {
    context : path.resolve(process.cwd(), 'src'),
    devtool : IS_PROD_MODE ? false : 'eval-cheap-source-map',
    mode    : IS_PROD_MODE ? 'production' : 'development',

    entry: {
      'main': ['./main.ts'],
    },

    // output bundles (location)
    output: {
        publicPath: '/static/frontend',
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].bundle.js',
    },

    resolve: {
      extensions: ['.ts', '.js', '.vue', '.json', '.css'],

      alias: {
        'vue': '@vue/runtime-dom',
      },

      plugins: [
        new TsconfigPathsPlugin(),
      ],
    },

    module: {
      rules: [
        {
          test : /\.ts$/,
          use: [
            'babel-loader',
            {
              loader: 'ts-loader',
              options: {
                appendTsSuffixTo: [/\.vue$/],
              },
            },
          ],
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            cssModules: {
              localIdentName: '[path][name]---[local]---[hash:base64:5]',
              camelCase: true
            }
          }
        },
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit : 8192,
            },
          },
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            "style-loader",
            "css-loader",
            "sass-loader",
          ],
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
        }
      ],
    },
    plugins: [
      new VueLoaderPlugin()
    ],
    devServer: {
        allowedHosts: 'all',
        hot: true,
        liveReload: true,
        compress: false,
        port: 8000,
    }
  }
}

