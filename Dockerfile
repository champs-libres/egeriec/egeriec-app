FROM node:17-alpine3.13 AS node
WORKDIR /frontend
COPY ./frontend/package*.json /frontend/
RUN npm ci
COPY ./frontend/ /frontend/
RUN npm run build-prod \
     && npm cache clean --force \
     && rm -rf node_modules

FROM python:alpine3.13
# Prevents Python from writing pyc files to disc (equivalent to python -B option)
ENV PYTHONDONTWRITEBYTECODE 1
# Prevents Python from buffering stdout and stderr (equivalent to python -u option)
ENV PYTHONUNBUFFERED 1

WORKDIR /backend
COPY ./backend/ /backend/
RUN mkdir -p /backend/src/egeriec/back/static/frontend/
COPY --from=node /frontend/dist/* /backend/src/egeriec/backend/static/frontend/


WORKDIR /backend
COPY ./backend/ /backend/
RUN mkdir -p /backend/src/egeriec/back/static/frontend/
COPY --from=node /frontend/dist/* /backend/src/egeriec/backend/static/frontend/

RUN apk add --no-cache postgresql-libs geos gdal postgresql-client libpq proj python3-dev gcc g++ make cmake gfortran openblas-dev \
     && python3 -m pip install --upgrade pip --no-cache-dir \
     && python3 -m pip install -r /backend/requirements.txt --no-cache-dir

EXPOSE 8000

WORKDIR /backend/src/egeriec

CMD ["gunicorn", "-b", "0.0.0.0:8000", "egeriec.wsgi:application"]
