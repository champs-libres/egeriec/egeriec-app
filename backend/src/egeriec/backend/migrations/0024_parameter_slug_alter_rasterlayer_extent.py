# Generated by Django 4.0.4 on 2022-10-14 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0023_rasterlayer_maplayer_raster'),
    ]

    operations = [
        migrations.AddField(
            model_name='parameter',
            name='slug',
            field=models.CharField(blank=True, max_length=70, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='rasterlayer',
            name='extent',
            field=models.CharField(blank=True, help_text='express the extent with numbers as "xmin, xmax, ymin, ymax"', max_length=75, null=True),
        ),
    ]
