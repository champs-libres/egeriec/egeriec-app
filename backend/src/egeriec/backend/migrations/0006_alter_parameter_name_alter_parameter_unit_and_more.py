# Generated by Django 4.0.4 on 2022-08-02 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("backend", "0005_station_aquiferkind_station_kind_station_length_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="parameter",
            name="name",
            field=models.CharField(max_length=75),
        ),
        migrations.AlterField(
            model_name="parameter",
            name="unit",
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="aquifer",
            field=models.CharField(blank=True, max_length=75, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="aquiferKind",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="depth",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="kind",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="length",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="municipality",
            field=models.CharField(blank=True, max_length=75, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="name",
            field=models.CharField(blank=True, max_length=75, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="preventionZone",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="station",
            name="strainerDepth",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="wfslayer",
            name="epsg",
            field=models.CharField(
                blank=True, help_text='e.g., "EPSG:4326"', max_length=10, null=True
            ),
        ),
        migrations.AlterField(
            model_name="wfslayer",
            name="layers",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="wfslayer",
            name="name",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="wmslayer",
            name="epsg",
            field=models.CharField(
                blank=True, help_text='e.g., "EPSG:4326"', max_length=10, null=True
            ),
        ),
        migrations.AlterField(
            model_name="wmslayer",
            name="layers",
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name="wmslayer",
            name="name",
            field=models.CharField(blank=True, max_length=75, null=True),
        ),
    ]
