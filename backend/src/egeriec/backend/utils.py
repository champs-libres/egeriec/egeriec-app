from django.utils.text import slugify

def simplify_name_for_slug(key: str):
    slugified_key = slugify(key)

    if slugified_key == 'efficacite-rendement-delimination-par-rapport-au-transfert-vers-la-ressource-ecoulement-diffus-superficiel':
        return 'efficacite-ecoulement-diffus-superficiel'
    elif slugified_key == 'efficacite-rendement-delimination-par-rapport-au-transfert-vers-la-ressource-ecoulement-diffus-de-surface':
        return 'efficacite-ecoulement-diffus-de-surface'    
    elif slugified_key == 'bande-lignocellulosique-de-saulepeuplier':
        return 'b-lignocellulosique-de-saulepeuplier'
    elif slugified_key == 'bande-lignocellulosique-de-miscanthus':
        return 'b-lignocellulosique-de-miscanthus'
    else:
        return slugified_key