from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField


class Station(models.Model):
    """
    Model for Stations
    """

    name = models.CharField(max_length=75, blank=True, null=True)
    active = models.BooleanField(default=True)
    center = models.PointField(srid=4326, blank=True, null=True)
    zone = models.MultiPolygonField(srid=4326, blank=True, null=True)
    municipality = models.CharField(max_length=75, blank=True, null=True)
    elevation = models.IntegerField(blank=True, null=True)
    depth = models.CharField(max_length=25, blank=True, null=True)
    strainerDepth = models.CharField(max_length=25, blank=True, null=True)
    length = models.CharField(max_length=25, blank=True, null=True)
    service = models.BooleanField(default=True)
    aquifer = models.CharField(max_length=75, blank=True, null=True)
    aquiferKind = models.CharField(max_length=25, blank=True, null=True)
    kind = models.CharField(max_length=25, blank=True, null=True)
    preventionZone = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return str(self.name)

class StationLandUse(models.Model):
    """
    Model for land-use information about Stations
    """

    station = models.OneToOneField(
        Station,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    surfaceZone = models.FloatField(blank=True, null=True)
    agriShare = models.FloatField(blank=True, null=True)
    cropShare = models.FloatField(blank=True, null=True)
    meadowShare = models.FloatField(blank=True, null=True)
    forestShare = models.FloatField(blank=True, null=True)
    builtShare = models.FloatField(blank=True, null=True)
    buildingsAge = models.FloatField(blank=True, null=True)
    precipitation = models.FloatField(blank=True, null=True)
    slope = models.FloatField(blank=True, null=True)
    numberCemeteries = models.IntegerField(blank=True, null=True)
    numberAgriBuildings = models.IntegerField(blank=True, null=True)
    numberNonConnectedBuildings = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return str(self.station.name)


PARAMETER_SCOPES_CHOICES = [
    ("dia", "diagnostic"),
    ("aod", "aod"),
]


class Parameter(models.Model):
    """
    Model for Parameters (the pollutant)
    """

    name = models.CharField(max_length=75)
    unit = models.CharField(max_length=15, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    min_value = models.FloatField(blank=True, null=True)
    max_value = models.FloatField(blank=True, null=True)
    threshold = models.FloatField(blank=True, null=True)
    ordering = models.IntegerField(blank=True, null=True)
    slug = models.CharField(max_length=70, unique=True, blank=True, null=True)
    scope = ArrayField(
        models.CharField(
            choices=PARAMETER_SCOPES_CHOICES,
            max_length=3,
        ),
    )

    def __str__(self):
        return f"{self.name} ({self.scope})"


class Record(models.Model):
    """
    Model for Records
    """

    value = models.FloatField(blank=True, null=True)
    datetime = models.DateTimeField()
    parameter = models.ForeignKey(Parameter, on_delete=models.CASCADE)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.parameter.name}-{self.value}"
