from django.contrib.gis.db import models

LAYER_CATEGORY = [
    ("drastic", "drastic"),
    ("pressure", "pressure"),
    ("info", "info"),
    ("tool-base", "tool-base"),
    ("tool-info", "tool-info"),
    ("tool-drawing", "tool-drawing"),
]

LAYER_TYPE = [
    ("wfs", "wfs"),
    ("wms", "wms"),
    ("geojson", "geojson"),
    ("raster", "raster"),
]

class GeojsonLayer(models.Model):
    """
    Model for geojson (postGIS) layer
    """

    name = models.CharField(max_length=75, blank=True, null=True)
    geom = models.MultiPolygonField(srid=4326, blank=True, null=True)
    visibility = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)

class RasterLayer(models.Model):
    """
    Model for a raster (static image) layer
    """

    name = models.CharField(max_length=75, blank=True, null=True)
    path = models.CharField(max_length=150, blank=True, null=True)
    extent = models.CharField(max_length=75, blank=True, null=True,
        help_text='express the extent with numbers as "xmin, xmax, ymin, ymax"'
    )
    visibility = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)

class WMSLayer(models.Model):
    """
    Model for WMS layer sources
    """

    name = models.CharField(max_length=75, blank=True, null=True)
    url = models.URLField()
    layers = models.CharField(max_length=50, blank=True, null=True)
    epsg = models.CharField(
        max_length=10, blank=True, null=True, help_text='e.g., "EPSG:4326"'
    )
    visibility = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)


class WFSLayer(models.Model):
    """
    Model for WFS layer sources
    """

    name = models.CharField(max_length=25, blank=True, null=True)
    url = models.URLField()
    layers = models.CharField(max_length=25, blank=True, null=True)
    epsg = models.CharField(
        max_length=10, blank=True, null=True, help_text='e.g., "EPSG:4326"'
    )
    visibility = models.BooleanField(default=True)
    params = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.name)

class MapLayer(models.Model):
    """
    Generic model for a map layer
    """

    name = models.CharField(max_length=75, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    category = models.CharField(
        choices=LAYER_CATEGORY,
        max_length = 32
    )
    type = models.CharField(
        choices=LAYER_TYPE,
        max_length = 32
    )
    style = models.TextField(blank=True, null=True)
    wfs = models.ForeignKey(
        WFSLayer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    wms = models.ForeignKey(
        WMSLayer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    geojson = models.ForeignKey(
        GeojsonLayer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    raster = models.ForeignKey(
        RasterLayer,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    visibility = models.BooleanField(default=True)
    minzoom = models.FloatField(null=True)

    def __str__(self):
        return str(self.name)

