from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify
from django.contrib.postgres.fields import ArrayField

from backend.utils import simplify_name_for_slug

from .record import Parameter

SECTION_CHOICES = [("ava", _("aval")), ("amt", _("amont")), ("mid", _("milieu"))]

METHODOLOGY_CHOICES = [("1", _("Methodology 1")), ("2", _("Methodology 2"))]

FORM_ACTION_CHOICES = [
    ("methodo1", _("Methodology 1")),
    ("methodo2", _("Methodology 2")),
    ("subform", _("Subform")),
    ("print", _("Text print"))
]

RESOURCE_TO_PROTECT_CHOICES = [
    ("surface", _("Eau surface")),
    ("souterraine", _("Eau souterraine")),
    ("amont", ("Amont")), # for the question Z.1.1
]


class TransfertMode(models.Model):
    """
    Model for TransfertMode
    """

    name = models.CharField(max_length=200)
    resources = ArrayField(
        models.CharField(
            choices=RESOURCE_TO_PROTECT_CHOICES,
            max_length=15,
        ),
    )
    form_action = models.CharField(
        choices=FORM_ACTION_CHOICES,
        max_length=10,
    )

    form_text = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.name


class SpatialConstraint(models.Model):
    """
    Model for SpatialConstraint

    A spacial constraint is a polygon where it is not allowed to add a solution
    """

    name = models.CharField(max_length=75)
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return self.name


class SolutionType(models.Model):
    """
    Model for SolutionType
    """

    name = models.CharField(max_length=75)
    slug = models.CharField(max_length=70, unique=True)
    description = models.TextField(null=True, blank=True)
    methodology = models.CharField(max_length=3, choices=METHODOLOGY_CHOICES)
    spatial_constraint = models.ForeignKey(
        SpatialConstraint, on_delete=models.CASCADE, null=True, blank=True
    )
    exigences_d_implantation = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, * args, ** kwargs):
        if self.slug is None or self.slug == '':
            self.slug = self.calculate_default_slug()
        super().save( * args, ** kwargs)


    def calculate_default_slug(self):
        slugified_name = simplify_name_for_slug(self.name)

        return f"{slugify(slugified_name)}_{self.methodology}"


class Criterion(models.Model):  # colonnes BCD de AMC1 & AMC2
    """
    Model for Criterion

    A criteria is a line (columns B,C,D) of the sheets AMC1/AMC2 of the excel OAD tool
    """

    name = models.CharField(max_length=150)
    slug = models.CharField(max_length=150, unique=True)
    category = models.CharField(max_length=75)
    subcategory = models.CharField(max_length=75)
    specificity = models.CharField(max_length=150, default="")

    def __str__(self):
        return self.slug

    def save(self, * args, ** kwargs):
        if self.slug is None or self.slug == '':
            self.slug = self.calculate_default_slug()
        super().save( * args, ** kwargs)


    def calculate_default_slug(self):
        slugified_name = simplify_name_for_slug(self.name)

        if self.specificity:
            return f"{slugify(slugified_name)}_{slugify(self.specificity)}"
        return f"{slugify(slugified_name)}"



class Cotation(models.Model):  # Donnée AMC1 & AMC2
    """
    Model for Cotation

    A cotation is a row in the F/M matrix of the sheets AMC1/AMC2 of the excell OAD tool
    """

    value = models.DecimalField(
        max_digits=5, decimal_places=4
    )  # , min_value=0, max_value=1) ou null (?)
    solution = models.ForeignKey(SolutionType, on_delete=models.CASCADE)
    criterion = models.ForeignKey(Criterion, on_delete=models.CASCADE)
    pollutant = models.ForeignKey( # se retouve dans criteria #TODO supprimer ou remplir apd de creitria
        Parameter, on_delete=models.CASCADE, null=True, blank=True
    )
    section = models.CharField(  # TODO supprimer ou remplir apd de creitria
        max_length=3, choices=SECTION_CHOICES, null=True, blank=True
    )
    methodology = models.CharField(max_length=3, choices=METHODOLOGY_CHOICES)
    # TODO unique solution, criteria, methodology


    """
class Scenario(models.Model):
    pass
    # ensemble de ScenarioLigne

class ScenarioLigne(models.Model):
    scenario = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    position = models.CharField(max_length=10) # CHOIX parcellaire / le_long_de_l_eau
    longueur = models.IntegerField()
    solution = models.CharField(max_length=10) # Bande enherbée
    # Haie
    # Haie + bande enherbée
    # Bande lignocellulosique de miscanthus
    # Bande lignocellulosique de saule
    # Bande lignocellulosique de peuplier
    # Ripisylve
    # Ripisylve + bande enherbée
    """