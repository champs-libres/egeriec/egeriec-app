from rest_framework_gis import serializers
from rest_framework import serializers as ser
from django.core.serializers import serialize
from backend.models import Parameter, Record, Station, StationLandUse, WMSLayer, WFSLayer, TransfertMode, Criterion, Cotation, MapLayer, GeojsonLayer, SolutionType

class ParameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parameter
        fields = (
            "name",
            "id",
            "unit",
            "description",
            "min_value",
            "max_value",
            "threshold",
            "ordering",
            "scope",
            "slug"
        )


class StationCenterSerializer(serializers.GeoFeatureModelSerializer):
    class Meta:
        model = Station
        geo_field = "center"
        fields = (
            "name",
            "id",
            "municipality",
            "elevation",
            "depth",
            "strainerDepth",
            "service",
            "aquifer",
            "aquiferKind",
            "kind",
            "preventionZone",
        )


class RecordSerializer(serializers.ModelSerializer):
    # parameter = ParameterSerializer(many=False, read_only=True)
    # station = StationCenterSerializer(many=False, read_only=True)
    class Meta:
        model = Record
        fields = ("id", "value", "parameter", "datetime", "station")


class StationZoneSerializer(serializers.GeoFeatureModelSerializer):
    class Meta:
        model = Station
        geo_field = "zone"
        fields = ("name", "id")

class StationLandUseSerializer(serializers.ModelSerializer):
    class Meta:
        model = StationLandUse
        fields = '__all__'

class MapLayerSerializer(serializers.ModelSerializer):

    data = ser.SerializerMethodField('get_data')

    def get_data(self, i):
        if i.type == "wms":
            return f"{i.wms.url}?LAYERS={i.wms.layers}"
        elif i.type == "raster":
            return f"{i.raster.path}?EXTENT={i.raster.extent}"
        elif i.type == "geojson":
            if i.geojson:
                return f"api/geojson-layers?id={i.geojson.id}"

    class Meta:
        model = MapLayer
        fields = ("id", "name", "description", "category", "type", "data", "style", "visibility", "minzoom")


class GeojsonLayerSerializer(serializers.GeoFeatureModelSerializer):
    class Meta:
        model = GeojsonLayer
        geo_field = "geom"
        fields = ("id", "name", "visibility")

class WMSLayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = WMSLayer
        fields = ("id", "name", "url", "layers", "epsg", "visibility")


class WFSLayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = WFSLayer
        fields = ("name", "url", "layers", "epsg", "visibility", "params")


class TransfertModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransfertMode
        fields = (
            "id",
            "name",
            "form_action",
            "form_text",
        )


class CriterionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Criterion
        fields = '__all__'



class CotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cotation
        fields = '__all__'

class SolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolutionType
        fields = '__all__'
