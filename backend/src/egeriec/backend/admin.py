from django.contrib import admin

from .models import (
    Record,
    Station,
    StationLandUse,
    WMSLayer,
    WFSLayer,
    SpatialConstraint,
    SolutionType,
    Criterion,
    Cotation,
    Parameter,
    TransfertMode,
    GeojsonLayer,
    RasterLayer,
    MapLayer
)

admin.site.register([Station, StationLandUse, WMSLayer, WFSLayer, GeojsonLayer, RasterLayer, SpatialConstraint])


class CotationAdmin(admin.ModelAdmin):
    list_display = ("value", "methodology", "solution", "criterion", "pollutant", "section")


class CriterionAdmin(admin.ModelAdmin):
    list_display = ("subcategory", "category", "name", "specificity", "slug")


class ParameterAdmin(admin.ModelAdmin):
    list_display = ("name", "unit", "threshold")


class RecordAdmin(admin.ModelAdmin):
    list_display = ("id", "datetime", "station", "parameter", "value")


class SolutionTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "get_methodology_display", "slug")


class TransfertModeAdmin(admin.ModelAdmin):
    list_display = ("name", "resources", "form_action", "form_text")
    ordering = ('pk',)

class MapLayerAdmin(admin.ModelAdmin):
    list_display = ("name", "category", "type", "visibility")

admin.site.register(Cotation, CotationAdmin)
admin.site.register(Criterion, CriterionAdmin)
admin.site.register(Parameter, ParameterAdmin)
admin.site.register(Record, RecordAdmin)
admin.site.register(SolutionType, SolutionTypeAdmin)
admin.site.register(TransfertMode, TransfertModeAdmin)
admin.site.register(MapLayer, MapLayerAdmin)