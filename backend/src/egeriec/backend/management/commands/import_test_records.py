from django.core.management.base import BaseCommand, CommandError
from backend.models import Record, Parameter, Station
import csv

"""
Command for importing a table of records ("Record") in csv format

exemple::

    python manage.py import_test_records -f ./backend/fixtures/test_data_records.csv

"""


class Command(BaseCommand):
    help = "Import records data from csv"

    def add_arguments(self, parser):

        parser.add_argument(
            "-f",
            "--filename",
            type=str,
            dest="filename",
            help="full path of the csv file to be imported",
            default="",
        )

    def handle(self, *args, **options):

        filename = options["filename"]
        print(filename)

        with open(filename, newline="") as csvfile:
            reader = csv.reader(csvfile, delimiter=",", quotechar='"')
            next(reader, None)  # skip the header
            for row in reader:
                parameter1 = Parameter.objects.get(
                    id=row[2]
                )  # TODO the parameter id is hardcoded in the csv, but we can retrieve it from a name
                parameter2 = Parameter.objects.get(id=row[5])
                station = Station.objects.get(
                    id=row[3]
                )  # TODO the station id is hardcoded in the csv, but we can retrieve it from a name

                Record.objects.create(
                    datetime=row[0], value=row[1], parameter=parameter1, station=station
                )

                Record.objects.create(
                    datetime=row[0], value=row[4], parameter=parameter2, station=station
                )
