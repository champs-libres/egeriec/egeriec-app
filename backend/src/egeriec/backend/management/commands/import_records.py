from django.core.management.base import BaseCommand, CommandError
from backend.models import Record, Parameter, Station
import csv

"""
Command for importing a table of records ("Record") in csv format

exemple::

    python manage.py import_records -f ./backend/fixtures/Mesures.csv

"""


class Command(BaseCommand):
    help = "Import records data from csv"

    def add_arguments(self, parser):

        parser.add_argument(
            "-f",
            "--filename",
            type=str,
            dest="filename",
            help="full path of the csv file to be imported",
            default="",
        )

    def handle(self, *args, **options):

        filename = options["filename"]
        print(filename)

        with open(filename, newline="") as csvfile:
            reader = csv.reader(csvfile, delimiter=",", quotechar='"')
            next(reader, None)  # skip the header
            for row in reader:
                try:
                    parameter = Parameter.objects.get(name=row[2])
                    station = Station.objects.get(name=row[1])

                    Record.objects.create(
                        datetime=row[0],
                        value=row[3],
                        parameter=parameter,
                        station=station,
                    )
                except Parameter.DoesNotExist:
                    print(f'Parameter "{row[2]}" non trouvé')
                except Station.DoesNotExist:
                    print(f'Station "{row[1]}" non trouvée')
