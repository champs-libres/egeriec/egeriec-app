from django.core.management.base import BaseCommand, CommandError
from backend.models import Record, Parameter, Station
import csv

from openpyxl import load_workbook

from ...models import Cotation, Criterion, SolutionType


class Command(BaseCommand):
    """
    Command for importing cotations for the excel tool

    exemple::

    python manage.py import_cotations -f ./backend/fixtures/Prototype220921.xlsx
    """

    help = "Import cotations data from excel tool"

    def add_arguments(self, parser):
        parser.add_argument(
            "-f",
            "--filename",
            type=str,
            dest="filename",
            help="full path of the excel file to be imported",
            default="",
        )

        parser.add_argument(
            "--no_data_only",
            action="store_true",
            help="to unactivate the data_only=True activated for openpyxl (formula value replaced by the value stored during the last read of the Excel sheet"
        )

    def handle(self, *args, **options):
        filename = options["filename"]
        no_data_only = options["no_data_only"]

        wb = load_workbook(filename = filename, data_only=(not no_data_only))
        # when data_only=True the formula are replaced by the value stored during the last read of the Excel sheet

        # handling page 'Données AMC (1)' (methodology 1)
        worksheet = wb["Données AMC (1)"]
        methodology = 1
        last_row_id = 50
        last_col_id = 12 #last column is M
        self.handle_sheet(worksheet, methodology, last_row_id, last_col_id)

        # handling page 'Données AMC (2)' (methodology 1)
        worksheet = wb["Données AMC (2)"]
        methodology = 2
        last_row_id = 53
        last_col_id = 13 #last column is M
        self.handle_sheet(worksheet, methodology, last_row_id, last_col_id)

    def handle_sheet(self, worksheet, methodology: int, last_row_id: int, last_col_id: int):
        """
        Handle an excel sheet containing the cotations for a methodology

        Solutions must stands in the row 3 from column F to last_col_id
        Criteria must stands in the matrix B4 x E__{last_row_id}
        The data must stands in the matrix : F4 x {last_col_id}__{last_row_id}
        So if last_col_id=12 and last_row_id=51, the data  are read in the matrix F4 x L51

        Parameters
        ----------
        worksheet: str
            the worksheet for the page "Données AMC (1)" or "Données AMC (2)"
        methodology: int
            the integer that represents the metodology (1 or 2)
        last_row_id:
            the id of the last row containing the data
        - last_col_id:
            the id of the last column containing the data
        """

        criterion_category = None
        criterion_subcategory = None
        criterion_name = None
        criterion_specificity = None

        criteria = [None] * 55
        for row_id, criterion_row in enumerate(worksheet.iter_rows(min_col=2, max_col=5, min_row=4, max_row=last_row_id), start=4):
            iter_category = criterion_row[0].value
            iter_subcategory = criterion_row[1].value
            iter_name =  criterion_row[2].value
            iter_specificity = criterion_row[3].value

            if iter_category:
                criterion_category = iter_category.strip()

            if iter_subcategory:
                criterion_subcategory = iter_subcategory.strip()

            if iter_name:
                criterion_name = iter_name.strip()

            criterion_specificity = "" if iter_specificity is None else iter_specificity

            criterion, _ = Criterion.objects.get_or_create(
                subcategory=criterion_subcategory,
                category=criterion_category,
                name=criterion_name,
                specificity=criterion_specificity
            )

            criteria[row_id] = criterion


        for solution_data_col in worksheet.iter_cols(min_col=6, max_col=last_col_id, min_row=3, max_row=last_row_id):
            solution_name = solution_data_col[0].value

            solution, _ = SolutionType.objects.get_or_create(
                name=solution_name,
                methodology=methodology
            )

            for row_id, data in enumerate(solution_data_col[1:], start=4): # solution_data_col va de 1 à n

                if data.data_type == 'f': # it is a formula (option data_only is False
                    print(f"Cell {data} is a formula -> passed : not value has been stored")
                else:
                    data_value = None if data.value == '-' or data.value == '/' else data.value

                    if data_value is not None:
                        cotation, _ = Cotation.objects.update_or_create(
                            solution=solution,
                            criterion=criteria[row_id],
                            section=None, #ici pour methodo1 TODO delete
                            methodology=methodology,
                            pollutant=None, # se trouve dans criterion si besoin TODO deleted
                            defaults={'value': data_value}
                        )
                        cotation.save()
