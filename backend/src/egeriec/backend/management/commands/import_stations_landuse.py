from django.core.management.base import BaseCommand, CommandError
from backend.models import StationLandUse, Station
import csv

"""
Command for importing a table of stations land-use info ("StationLandUse") in csv format

exemple::

    python manage.py import_stations_landuse -f ./backend/fixtures/Stations_landuse.csv

"""


class Command(BaseCommand):
    help = "Import stations land use data from csv"

    def add_arguments(self, parser):

        parser.add_argument(
            "-f",
            "--filename",
            type=str,
            dest="filename",
            help="full path of the csv file to be imported",
            default="",
        )

    def handle(self, *args, **options):

        filename = options["filename"]

        with open(filename, newline="") as csvfile:
            reader = csv.reader(csvfile, delimiter=",")
            next(reader, None)  # skip the header
            for row in reader:
                try:
                    station = Station.objects.get(name=row[0])
                    StationLandUse.objects.create(
                        station=station,
                        surfaceZone=row[1],
                        agriShare=row[2],
                        cropShare=row[3],
                        meadowShare=row[4],
                        forestShare=row[5],
                        builtShare=row[6],
                        buildingsAge=row[7],
                        precipitation=row[8],
                        slope=row[9],
                        numberCemeteries=int(float(row[10])),
                        numberAgriBuildings=int(float(row[11])),
                        numberNonConnectedBuildings=int(float(row[12]))
                    )
                except Station.DoesNotExist:
                    print(f'Station "{row[1]}" non trouvée')
