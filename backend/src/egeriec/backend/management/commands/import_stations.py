from django.core.management.base import BaseCommand, CommandError
from backend.models import Station
import csv

"""
Command for importing a table of stations ("Stations") in csv format

exemple::

    python manage.py import_stations -f ./backend/fixtures/Mes_Captages_4326.csv

"""


class Command(BaseCommand):
    help = "Import stations data from csv"

    def add_arguments(self, parser):

        parser.add_argument(
            "-f",
            "--filename",
            type=str,
            dest="filename",
            help="full path of the csv file to be imported",
            default="",
        )

    def handle(self, *args, **options):

        filename = options["filename"]
        print(filename)

        with open(filename, newline="") as csvfile:
            reader = csv.reader(csvfile, delimiter=",", quotechar='"')
            next(reader, None)  # skip the header
            for row in reader:
                Station.objects.create(
                    name=row[0],
                    active=True,
                    center=f"SRID=4326;POINT ({row[17]} {row[18]})",
                    municipality=row[1],
                    elevation=round(float(row[11])),
                    depth=row[4],
                    strainerDepth=row[5],
                    length=row[6],
                    service=True if row[2] == "ES" else False,
                    aquifer=row[8],
                    aquiferKind=row[7],
                    kind=row[3],
                    preventionZone=row[12],
                )
