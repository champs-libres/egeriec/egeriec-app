from django.urls import path
from backend.views.app import HomepageView

urlpatterns = [
    path("", HomepageView.as_view(), name="home"),
]
