from django.urls import path
from backend.views.api import (
    GetParameterView,
    GetRecordView,
    GetStationCenterView,
    GetStationZoneView,
    GetStationLandUseView,
    GetMapLayerView,
    GetGeojsonLayerView,
    GetWMSLayerView,
    GetWFSLayerView,
    GetTransfertModesView,
    GetCotationView,
    GetCriterionView,
    GetSolutionView,
    get_cotations_by_solution_and_criterion,
)

urlpatterns = [
    path("parameters/", GetParameterView.as_view()),
    path("stations/", GetStationCenterView.as_view()),
    path("stations-zones/", GetStationZoneView.as_view()),
    path("stations-landuse/", GetStationLandUseView.as_view()),
    path("map-layers/", GetMapLayerView.as_view()),
    path("geojson-layers/", GetGeojsonLayerView.as_view()),
    path("wms-layers/", GetWMSLayerView.as_view()),
    path("wfs-layers/", GetWFSLayerView.as_view()),
    path("records/", GetRecordView.as_view()),
    path("transferModes/", GetTransfertModesView.as_view()),
    path("criteria/", GetCriterionView.as_view()),
    path("solutions/", GetSolutionView.as_view()),
    path("cotations/", GetCotationView.as_view()),
    path("cotations_by_solution_and_criterion/", get_cotations_by_solution_and_criterion),
]
