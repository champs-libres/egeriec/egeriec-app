from django.urls import path
from backend.views.smooth import get_smooth

urlpatterns = [
    path('smooth/', get_smooth),
]