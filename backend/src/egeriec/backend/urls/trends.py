from django.urls import path
from backend.views.trends import get_trend

urlpatterns = [
    path('trends/', get_trend),
]