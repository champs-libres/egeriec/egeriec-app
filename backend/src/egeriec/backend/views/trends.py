
from django.http import JsonResponse

from backend.models import Parameter, Record, Station
from backend.computation.trends import compute_trend

def get_trend(request):
    """ Get a trend from a time-series of data. Gives a value and a slope for n dates where the trend can be computed."""

    station_id = request.GET.get('station', None)
    parameter_id = request.GET.get('parameter', None)

    station = Station.objects.get(id=station_id)
    parameter = Parameter.objects.get(id=parameter_id)

    lines = compute_trend(station, parameter)

    trends = list()
    for i in lines.get('Date'):
       trends.append({
          'date': lines.get('Date')[i],
          'intercept': lines.get('Estimated_value')[i],
          'slope': lines.get('Slope')[i],
          'significance': lines.get('Significance')[i]
       })

    return JsonResponse({
        'count': len(trends),
        'results': trends
    })
