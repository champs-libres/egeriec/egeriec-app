from rest_framework.generics import ListAPIView
from django.http import HttpResponse
from django.core.serializers import serialize


from django.http import JsonResponse

from backend.serializers import (
    ParameterSerializer,
    RecordSerializer,
    StationCenterSerializer,
    StationZoneSerializer,
    StationLandUseSerializer,
    TransfertModeSerializer,
    MapLayerSerializer,
    GeojsonLayerSerializer,
    WMSLayerSerializer,
    WFSLayerSerializer,
    CriterionSerializer,
    CotationSerializer,
    SolutionSerializer
)

import json

from backend.models import Parameter, Record, Station, StationLandUse, WMSLayer, WFSLayer, TransfertMode, Criterion, Cotation, MapLayer, GeojsonLayer, SolutionType

class GetParameterView(ListAPIView):
    queryset = Parameter.objects.all()
    serializer_class = ParameterSerializer


class GetRecordView(ListAPIView):
    serializer_class = RecordSerializer

    def get_queryset(self):
        queryset = Record.objects.all()

        station = self.request.query_params.get("station")
        parameter = self.request.query_params.get("parameter")

        if station is not None:
            queryset = queryset.filter(station__id=station)

        if parameter is not None:
            queryset = queryset.filter(parameter__id=parameter)

        return queryset


class GetStationCenterView(ListAPIView):
    queryset = Station.objects.all()
    serializer_class = StationCenterSerializer

    def get_queryset(self):
        return Station.objects.filter(active=True)


class GetStationZoneView(ListAPIView):
    queryset = Station.objects.all()
    serializer_class = StationZoneSerializer

class GetStationLandUseView(ListAPIView):
    queryset = StationLandUse.objects.all()
    serializer_class = StationLandUseSerializer

class GetMapLayerView(ListAPIView):
    queryset = MapLayer.objects.all()
    serializer_class = MapLayerSerializer


class GetGeojsonLayerView(ListAPIView):
    serializer_class = GeojsonLayerSerializer

    def get_queryset(self):
        queryset = GeojsonLayer.objects.all()

        id = self.request.query_params.get("id")

        if id is not None:
            queryset = queryset.filter(id=id)

        return queryset


class GetWMSLayerView(ListAPIView):
    queryset = WMSLayer.objects.all()
    serializer_class = WMSLayerSerializer

class GetWFSLayerView(ListAPIView):
    queryset = WFSLayer.objects.all()
    serializer_class = WFSLayerSerializer


class GetTransfertModesView(ListAPIView):
    serializer_class = TransfertModeSerializer

    def get_queryset(self):
        queryset = TransfertMode.objects.all()

        resource = self.request.query_params.get("resource")
        if resource:

            queryset = queryset.filter(resources__overlap=resource.split(','))

        return queryset


class GetCriterionView(ListAPIView):
    queryset = Criterion.objects.all()
    serializer_class = CriterionSerializer

class GetSolutionView(ListAPIView):
    queryset = SolutionType.objects.all()
    serializer_class = SolutionSerializer


def get_cotations_by_solution_and_criterion(request):
    """Get coations by solutions and criterions"""

    solutions = SolutionType.objects.all()

    cotations_by_solution_and_criterion = dict()


    for solution in solutions:
        cotations_by_solution_and_criterion[solution.slug] = dict()

        for cotation in Cotation.objects.filter(solution=solution):
            cotations_by_solution_and_criterion[solution.slug][cotation.criterion.slug] = {
                'value': cotation.value
            }


    return JsonResponse(cotations_by_solution_and_criterion, safe=False)


class GetCotationView(ListAPIView):
    """
    Get location by criterion, solution & methodology
    TODO change with slug
    TODO check only return one object
    """
    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """

        raise Error('Ne pas utiliser')

        # methodology = 1
        # par exemple bande enherbe ->

        #  bloucle dessus (solution de M1)

        # criterion = self.kwargs['criterion'] # No3 (nitrates) x Efficacite x ecoulement diffus surface

        # boucle que les spicifictés des efficacitéss (ici polluant)

        return Cotation.objects.filter(solution__slug='bande-enherbee_1', methodology="1") # ici pbm methodo redondant

        # return Cotation.objects.filter(criterion__id=criterion, solution__id=solution, methodology=methodology)

    serializer_class = CotationSerializer