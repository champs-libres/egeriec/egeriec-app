
from django.http import JsonResponse

from backend.models import Parameter, Record, Station
from backend.computation.lowess_smoother import compute_lowess

def get_smooth(request):
    """ Get a smooth line from a time-series of data."""

    station_id = request.GET.get('station', None)
    parameter_id = request.GET.get('parameter', None)

    station = Station.objects.get(id=station_id)
    parameter = Parameter.objects.get(id=parameter_id)

    lines = compute_lowess(station, parameter)

    smooth = list()
    for k,v in lines['lowess'].items():
       smooth.append({
          'date': k,
          'value': round(v, 3),
       })

    return JsonResponse({
      'count': len(smooth),
      'results': smooth
    })
