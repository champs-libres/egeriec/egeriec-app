from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

class HomepageView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"
    login_url = '/admin/login/'
