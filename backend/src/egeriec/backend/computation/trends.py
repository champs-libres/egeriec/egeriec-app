"""
Created on Tue Aug  9 11:31:14 2022

@author: verstraetene
"""


try:
    import numpy as np
    import pandas as pd
    import ruptures as rpt
    from scipy.stats import theilslopes, kendalltau
except ImportError:
    print('error importing numpy, pandas, ruptures or scipy')



from backend.models import Record

def get_bkps(time_series, cost_function, pen):
    """
    Finds the optimal location of a given number of breakpoints in a time series using the specified cost function.

    Parameters
    ----------
    time_series : Series
        time series for which the breakpoints should be found.
    cost_function : 'clinear' or 'linear'
        model type to use in the dynamic programming algorithm.
    number_bkps : int
        number of breakpoints to define in the time series.

    Returns
    -------
    list
        the integer-location based indices of the breakpoints.

    """
    signal = np.array(time_series)
    algo = rpt.Binseg(model=cost_function, min_size=5*365, jump=30).fit(np.column_stack((signal.reshape(-1, 1), np.arange(len(signal)))))
    my_bkps = algo.predict(pen=pen)
    return my_bkps[:-1]

def get_ypred_bkps(time_series, bkps):
    """
    Calculates the y values of the linear regressions between the specified breaking points, with a continuity constraint.

    Parameters
    ----------
    time_series : Series
        time series that is approximated with the linear regressions.
    bkps : list
        integer-location based indices of the breakpoints.

    Returns
    -------
    array
        y values of the linear segments between the breaking points.

    """
    signal = np.array(time_series)
    ypred=[]
    start=0
    for bkp in bkps + [len(time_series)-1]:
        slope = (signal[bkp]-signal[start])/(bkp-start)
        intercept = signal[start]
        ypred_part = slope*np.arange(bkp-start)+intercept
        ypred = np.hstack((ypred, ypred_part))
        start=bkp
    ypred = np.hstack((ypred, [signal[bkp]]))
    return ypred

def get_BIC(RSS, n, k):
    """
    Calculates the Bayesian Information Criterion for a residual sum of squares RSS,
    a sample size of n, and k breaking points (including the first point of the time series)

    """
    return np.log(RSS/n)+2*k*np.log(n)/n

def get_ypred_sen(time_series_daily, time_series_measures, bkps):
    """
    Calculates the Theil Sen regression of all segments and returns the intercept and slope for each segment

    Parameters
    ----------
    time_series_daily : Series
        time series (at daily time step) to be approximated with the Theil Sen regressions.
    bkps : list
        integer-location based indices of the breakpoints.

    Returns
    -------
    intercepts : list
        intercepts of all segments.
    slopes : list
        slopes of all segments.
    significant : list
        True if slope is significant, False if slope is not significant
    """
    #get the y values of the sen slope of the segments between the specified breaking points
    signal = np.array(time_series_daily)
    intercepts=[]
    slopes=[]
    significant=[]
    start=0
    for bkp in bkps + [len(time_series_daily)-1]:
        res = theilslopes(signal[start:bkp], np.arange(bkp-start))
        intercepts.append(res[1])
        slopes.append(res[0])
        _, pvalue_kt = kendalltau(time_series_measures.index[start:bkp], time_series_measures[start:bkp], nan_policy='omit')
        significant.append(True if pvalue_kt < 0.05 else False)
        start=bkp
    return intercepts, slopes, significant

def compute_trend(station, parameter):
    """
    Compute the trend in the record time series for a given station and parameter.
    Output a dict of 1 or more trending lines over the time series.
    """

    records = Record.objects.filter(station=station, parameter=parameter).order_by('datetime')

    param_values = [r.value for r in records]
    datetime_values = [r.datetime.date() for r in records]
    captage_data = pd.Series(data=param_values, index=datetime_values, name='Valeur')

    daily_data = captage_data.reindex(pd.date_range(str(captage_data.index[0].year), str(captage_data.index[-1].year+1), freq='D')[:-1]) #change the index column to a daily time step
    daily_nona_data = daily_data.interpolate(method='linear').dropna() #interpolate missing data and delete front and end missing data
    ## Find the best number of breaking points and their dates
    # best_bic = {'number':-1, 'score':np.inf, 'bkps':[]} #current best number of bkps is stored in this dict
    # max_bkp = len(daily_nona_data)/(5*365)-1 #the maximum number of breaking points is one every five years
    # for n_bkps in np.arange(0,max_bkp):
    #     new_bkps = get_bkps(daily_nona_data, 'clinear', n_bkps) #get the optimal location for the bkps if n_bkps
    #     new_ypred = get_ypred_bkps(daily_nona_data, new_bkps) #get the estimated yvalues for the selected bkps
    #     new_bic = get_BIC(RSS=np.sum(np.square(new_ypred-daily_nona_data)), n=captage_data.count(), k=n_bkps+1) #get the BIC value associated to the estimated values for y
    #     if new_bic < best_bic['score']: #replace by best bic
    #         best_bic['score'] = new_bic
    #         best_bic['number'] = n_bkps
    #         best_bic['bkps'] = new_bkps
    # #Once the bkps are found, calculate the sen slopes between the bkps
    # ypred_sen, slopes_sen, significant = get_ypred_sen(daily_nona_data, daily_data, best_bic['bkps'])

    my_bkps = get_bkps(daily_nona_data, 'clinear', pen=3) #get the optimal location for the bkps if n_bkps
    ypred_sen, slopes_sen, significant = get_ypred_sen(daily_nona_data, daily_data, my_bkps)

    #store all results in the output dataframe
    tmp_df = pd.DataFrame()
    tmp_df['Date'] = daily_nona_data.iloc[[0]+my_bkps].index
    tmp_df['Estimated_value'] = ypred_sen
    tmp_df['Slope'] = [el*365 for el in slopes_sen]
    tmp_df['Significance'] = significant

    return tmp_df.to_dict()
