# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 09:18:54 2022

@author: verstraetene
"""

## only use for pesticides and nitrates, not relevant for turbidity
try:
    import numpy as np
    import pandas as pd
    import statsmodels.api as sm
except ImportError:
    print('error importing numpy, pandas or statsmodels')

from backend.models import Record

def aggregate_monthly(my_df):
    my_df['year'] = my_df.index.year
    my_df['month'] = my_df.index.month
    my_df['date_month'] = pd.to_datetime(my_df['year'].apply(str)+'-'+my_df['month'].apply(str))
    my_df = my_df.groupby('date_month').mean()
    new_index  = pd.date_range(my_df.index[0], my_df.index[-1], freq='MS')
    my_df = my_df.reindex(new_index).interpolate()
    del my_df['year'], my_df['month']
    my_df.index.names = ['Date']
    return my_df

def compute_lowess(station, parameter):

    records = Record.objects.filter(station=station, parameter=parameter).order_by('datetime')

    #n_points_max = 12*5 # maximum number of days for the moving window of the lowess smoother
    n_points_max = 365 * 5
    frac_max = 2./3 # minimum fraction of the time series length for the moving window of the lowess smoother

    param_values = [r.value for r in records]
    datetime_values = [r.datetime.date() for r in records]
    captage_data = pd.Series(data=param_values, index=datetime_values, name='Valeur')

    # # prepare data
    captage_data_daily = captage_data.reindex(pd.date_range(str(captage_data.index[0].year), str(captage_data.index[-1].year+1), freq='D')[:-1])
    captage_data_daily = captage_data_daily.interpolate(method='linear', limit_area='inside').dropna() #interpolate missing data and delete front and end missing data

    # captage_data_daily = pd.DataFrame(data=param_values, index=datetime_values, columns=['Valeur'])
    # captage_data_monthly = aggregate_monthly(captage_data_daily)
    # lowess smoother
    lowess = sm.nonparametric.lowess
    # frac = np.minimum(n_points_max/len(captage_data_monthly), frac_max)
    # smoothed_data = lowess(captage_data_monthly.values[:,0], captage_data_monthly.index, frac=frac, delta=3)
    # smoothed_data_df = pd.DataFrame(smoothed_data[:,1], index=captage_data_monthly.index, columns=['lowess'])
    frac = np.minimum(n_points_max/len(captage_data_daily), frac_max)
    smoothed_data = lowess(captage_data_daily, captage_data_daily.index, frac=frac, delta=100)
    smoothed_data_df = pd.DataFrame(smoothed_data[:,1], index=captage_data_daily.index, columns=['lowess'])

    return smoothed_data_df.to_dict()
