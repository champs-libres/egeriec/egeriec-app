"""egeriec URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from backend.views.app import HomepageView

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'api/', include('backend.urls.api')),
    path(r'decision-tool/', include('backend.urls.app')),
    path(r'', include('backend.urls.app')),
    path(r'', include('backend.urls.trends')),
    path(r'', include('backend.urls.smooth')),
    path('<int>/<string>/', include('backend.urls.app'))
]

urlpatterns += staticfiles_urlpatterns()
